﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace IndicadoresLaboral.Clases
{
    public class Conexion
    {

        static MySqlConnection conexion;

        // Apertura de conexion
        public static MySqlConnection Conecta(String dba)
        {
            try
            {
                conexion = new MySqlConnection(System.Configuration.ConfigurationManager.AppSettings[dba]);
                conexion.Open();
                return conexion;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error al iniciar la conexion: " + e.Message);
                return null;
            }
        }

        // Cierre de conexion
        public static void Cierra(MySqlConnection c)
        {
            try
            {
                c.Dispose();
                c.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error al cerrar la conexion: " + e.Message);
            }
        }


        public static MySqlTransaction IniciaTransaccion(MySqlConnection c)
        {
            try
            {
                return c.BeginTransaction();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error al Inicia transaccion: " + e.Message);
                return null;
            }
        }
        
    }
}