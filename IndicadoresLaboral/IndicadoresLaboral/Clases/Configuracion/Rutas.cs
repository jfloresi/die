﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace IndicadoresLaboral.Clases.Configuracion
{
    public class Rutas
    {

        public static void RegistraRutas(RouteCollection routes)
        {

            routes.MapPageRoute("Principal", "", "~/Laboral/Principal.aspx");
            routes.MapPageRoute("Logueo", "Login", "~/Laboral/Login.aspx");

        }

    }
}