﻿
using System.Web.Optimization;

namespace IndicadoresLaboral.Clases.Configuracion
{
    public class Bundles
    {

        public static void RegisterBundles(BundleCollection bundles)
        {
            //INICIOSESION

            bundles.Add(new ScriptBundle("~/Js").Include(
                        "~/Scripts/jquery-3.5.1.min.js",
                        "~/Scripts/umd/popper.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Util/PNotify/pnotify.custom.min.js",
                        "~/Util/Chats/chart.js",
                        "~/Util/PDFs/html2canvas.min.js",
                        "~/Util/PDFs/jspdf.min.js",
                        "~/Laboral/Js/General.js",
                        "~/Laboral/Js/Principal.js",
                        "~/Laboral/Js/Indicadores.js"));

            bundles.Add(new StyleBundle("~/Css").Include(
                        "~/Content/bootstrap.min.css",
                        "~/Util/PNotify/pnotify.custom.min.css",
                        "~/Util/PNotify/animate.css",
                        "~/Util/FontAwesome/css/all.css",
                        "~/Laboral/Css/Principal.css",
                        "~/Laboral/Css/Indicadores.css"));


        BundleTable.EnableOptimizations = true;
        }

    }
}