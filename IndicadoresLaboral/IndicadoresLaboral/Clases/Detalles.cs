﻿using MySql.Data.MySqlClient;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace IndicadoresLaboral.Clases
{
    public class Detalles
    {
        MySqlConnection c = null;
        MySqlCommand cmd = null;
        string[] mesinyear = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };

        public ExcelPackage GeneraExcel(int anio, int mes, String distrito, String juzgado)
        {
            ExcelPackage d = new ExcelPackage();
            try
            {
                
                ExcelWorksheet quejas = d.Workbook.Worksheets.Add("QUEJAS");
                GeneraHojaUno(quejas, anio, mes, distrito, juzgado);

                ExcelWorksheet acu = d.Workbook.Worksheets.Add("PROMOCIONES ACORDADAS");
                GeneraHojaDos(acu, anio, mes, distrito, juzgado);

                ExcelWorksheet noti = d.Workbook.Worksheets.Add("NOTIFICACIONES PERSONALES");
                GeneraHojaTres(noti, anio, mes, distrito, juzgado);

                ExcelWorksheet aud = d.Workbook.Worksheets.Add("AUDIENCIAS CELEBRADAS");
                GeneraHojaCuatro(aud, anio, mes, distrito, juzgado);

                ExcelWorksheet resol = d.Workbook.Worksheets.Add("RESOLUCIONES");
                GeneraHojaCinco(resol, anio, mes, distrito, juzgado);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error al generar excel : " + e.Message + "\n" + e.StackTrace);
            }

            return d;
        }

        private void GeneraHojaUno(ExcelWorksheet x, int anio, int mes, String distrito, String juzgado)
        {

            DateTime ini = new DateTime(anio, mes, 1);
            DateTime fin = ini.AddMonths(1).AddDays(-1);

            x.Cells["B1:F1"].Merge = true;
            x.Cells["B2:F2"].Merge = true;
            x.Cells["B5:F5"].Merge = true;

            x.Cells["B1"].Value = "Fuente de información: EXLAB";
            x.Cells["B2"].Value = "Periodo de información: " +
                (ini.ToString("dd \\de MMMM \\de yyyy", CultureInfo.CreateSpecificCulture("es-Mx")))
                + " al " + (fin.ToString("dd \\de MMMM \\de yyyy", CultureInfo.CreateSpecificCulture("es-Mx")));

            x.Cells["B5"].Value = "QUEJAS DEL SERVICIO DE ATENCIÓN AL PUBLICO";

            int rg = 6;

            x.Cells["B6"].Value = "AÑO";
            x.Cells["C6"].Value = "MES";
            x.Cells["D6"].Value = "JUZGADO";
            x.Cells["E6"].Value = "ID QUEJA";
            x.Cells["F6"].Value = "FECHA QUEJA";

            x.Cells["B5:F6"].Style.Font.Bold = true;
            x.Cells["B5:F6"].Style.Font.Color.SetColor(Color.White);
            x.Cells["B5:F6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            x.Cells["B5:F6"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#CC0000"));
            x.Cells["B5:F6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            try
            {
                c = Conexion.Conecta("dba");

                cmd = new MySqlCommand("select * from indicadores_pjem_laboral.tblquejasdetalle q " +
                    "inner join indicadores_pjem_laboral.tbljuzgados j on j.cvejuzgado = q.cvejuzgado " +
                    "where anio = @anioc and mes = @mesc and j.cvedistrito like @distritoc and j.cvejuzgado like @cvejuzgadoc ", c);
                cmd.Parameters.AddWithValue("@anioc", anio);
                cmd.Parameters.AddWithValue("@mesc", mes);
                cmd.Parameters.AddWithValue("@distritoc", "%" + distrito + "%");
                cmd.Parameters.AddWithValue("@cvejuzgadoc", "%" + juzgado + "%");

                MySqlDataReader r = cmd.ExecuteReader();

                while (r.Read())
                {
                    rg += 1;
                    x.Cells["B" + rg].Value = r.GetString("anio");
                    x.Cells["C" + rg].Value = mesinyear[r.GetInt16("mes") - 1];
                    x.Cells["D" + rg].Value = r.GetString("nomJuzgado");
                    x.Cells["E" + rg].Value = r.GetString("idQueja");
                    x.Cells["F" + rg].Value = r.GetDateTime("fecha").ToString("dd/MM/yyyy HH:mm:ss");

                }

            }
            catch (MySqlException e)
            {
                Debug.WriteLine("Hoja indicador Quejas: " + e.Message);
            }
            finally
            {
                c.Dispose();
                Conexion.Cierra(c);
            }


            x.Cells["B5:F" + rg].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:F" + rg].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:F" + rg].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:F" + rg].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:F" + rg].AutoFitColumns();

        }

        private void GeneraHojaDos(ExcelWorksheet x, int anio, int mes, String distrito, String juzgado)
        {

            DateTime ini= new DateTime(anio, mes, 1);
            DateTime fin = ini.AddMonths(1).AddDays(-1);

            x.Cells["B1:I1"].Merge = true;
            x.Cells["B2:I2"].Merge = true;
            x.Cells["B5:I5"].Merge = true;

            x.Cells["B1"].Value = "Fuente de información: EXLAB";
            x.Cells["B2"].Value = "Periodo de información: " +
                (ini.ToString("dd \\de MMMM \\de yyyy", CultureInfo.CreateSpecificCulture("es-Mx")))  
                + " al " + (fin.ToString("dd \\de MMMM \\de yyyy", CultureInfo.CreateSpecificCulture("es-Mx"))) ;

            x.Cells["B5"].Value = "TIEMPO PARA EL ACUERDO DE ACTUACIONES";

            int rg = 6;

            x.Cells["B6"].Value = "AÑO";
            x.Cells["C6"].Value = "MES";
            x.Cells["D6"].Value = "JUZGADO";
            x.Cells["E6"].Value = "ID PROMOCION";
            x.Cells["F6"].Value = "ID ACUERDO";
            x.Cells["G6"].Value = "FECHA PROMOCION";
            x.Cells["H6"].Value = "FECHA ACUERDO";
            x.Cells["I6"].Value = "HORAS";

            x.Cells["B5:I6"].Style.Font.Bold = true;
            x.Cells["B5:I6"].Style.Font.Color.SetColor(Color.White);
            x.Cells["B5:I6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            x.Cells["B5:I6"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#CC0000"));
            x.Cells["B5:I6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            try
            {
                c = Conexion.Conecta("dba");

                cmd = new MySqlCommand("select * from indicadores_pjem_laboral.tblacuerdosdetalle ac " +
                    "inner join indicadores_pjem_laboral.tbljuzgados j on j.cvejuzgado = ac.cveJuzgado " +
                    "where anio = @anioc and mes = @mesc and j.cvedistrito like @distritoc and j.cvejuzgado like @cvejuzgadoc ", c);
                cmd.Parameters.AddWithValue("@anioc", anio);
                cmd.Parameters.AddWithValue("@mesc", mes);
                cmd.Parameters.AddWithValue("@distritoc", "%"+distrito+"%");
                cmd.Parameters.AddWithValue("@cvejuzgadoc", "%"+juzgado+"%");

                MySqlDataReader r = cmd.ExecuteReader();

                while (r.Read())
                {
                    rg += 1;
                    x.Cells["B" + rg].Value = r.GetString("anio");
                    x.Cells["C" + rg].Value = mesinyear[r.GetInt16("mes")-1];
                    x.Cells["D" + rg].Value = r.GetString("nomJuzgado");
                    x.Cells["E" + rg].Value = r.GetString("idPromocion");
                    x.Cells["F" + rg].Value = r.GetString("idAcuerdo");
                    x.Cells["G" + rg].Value = r.GetDateTime("fechaPromocion").ToString("dd/MM/yyyy HH:mm:ss");
                    x.Cells["H" + rg].Value = r.GetDateTime("fechaAcuerdo").ToString("dd/MM/yyyy HH:mm:ss");
                    x.Cells["I" + rg].Value = r.GetString("diferencia");
                   

                }

            }
            catch (MySqlException e)
            {
                Debug.WriteLine("Hoja indicador acuerdos: " + e.Message);
            }
            finally
            {
                c.Dispose();
                Conexion.Cierra(c);
            }


            x.Cells["B5:I"+rg].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:I"+rg].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:I"+rg].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:I"+rg].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:I"+rg].AutoFitColumns();

        }

        private void GeneraHojaCuatro(ExcelWorksheet x, int anio, int mes, String distrito, String juzgado)
        {

            DateTime ini = new DateTime(anio, mes, 1);
            DateTime fin = ini.AddMonths(1).AddDays(-1);

            x.Cells["B1:H1"].Merge = true;
            x.Cells["B2:H2"].Merge = true;
            x.Cells["B5:H5"].Merge = true;

            x.Cells["B1"].Value = "Fuente de información: EXLAB";
            x.Cells["B2"].Value = "Periodo de información: " +
                (ini.ToString("dd \\de MMMM \\de yyyy", CultureInfo.CreateSpecificCulture("es-Mx")))
                + " al " + (fin.ToString("dd \\de MMMM \\de yyyy", CultureInfo.CreateSpecificCulture("es-Mx")));

            x.Cells["B5"].Value = "PORCENTAJE DE AUDIENCIAS CELEBRADAS";

            int rg = 6;

            x.Cells["B6"].Value = "AÑO";
            x.Cells["C6"].Value = "MES";
            x.Cells["D6"].Value = "JUZGADO";
            x.Cells["E6"].Value = "ID AUDIENCIA";
            x.Cells["F6"].Value = "ESTATUS AUDIENCIA";
            x.Cells["G6"].Value = "FECHA INICIO AUDIENCIA";
            x.Cells["H6"].Value = "FECHA TERMINO AUDIENCIA";

            x.Cells["B5:H6"].Style.Font.Bold = true;
            x.Cells["B5:H6"].Style.Font.Color.SetColor(Color.White);
            x.Cells["B5:H6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            x.Cells["B5:H6"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#CC0000"));
            x.Cells["B5:H6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            try
            {
                c = Conexion.Conecta("dba");

                cmd = new MySqlCommand("select * from indicadores_pjem_laboral.tblaudienciasdetalle a " +
                    "inner join indicadores_pjem_laboral.tbljuzgados j on j.cvejuzgado = a.cvejuzgado " +
                    "where anio = @anioc and mes = @mesc and j.cvedistrito like @distritoc and j.cvejuzgado like @cvejuzgadoc ", c);
                cmd.Parameters.AddWithValue("@anioc", anio);
                cmd.Parameters.AddWithValue("@mesc", mes);
                cmd.Parameters.AddWithValue("@distritoc", "%" + distrito + "%");
                cmd.Parameters.AddWithValue("@cvejuzgadoc", "%" + juzgado + "%");

                MySqlDataReader r = cmd.ExecuteReader();

                while (r.Read())
                {
                    rg += 1;
                    x.Cells["B" + rg].Value = r.GetString("anio");
                    x.Cells["C" + rg].Value = mesinyear[r.GetInt16("mes") - 1];
                    x.Cells["D" + rg].Value = r.GetString("nomJuzgado");
                    x.Cells["E" + rg].Value = r.GetString("idAudiencia");
                    x.Cells["F" + rg].Value = r.GetString("EstatusAudiencia");
                    x.Cells["G" + rg].Value = r.GetDateTime("fechaInicioAudiencia").ToString("dd/MM/yyyy HH:mm:ss");
                    x.Cells["H" + rg].Value = r.GetDateTime("fechaTerminoAudiencia").ToString("dd/MM/yyyy HH:mm:ss");

                }

            }
            catch (MySqlException e)
            {
                Debug.WriteLine("Hoja indicador audiencias: " + e.Message);
            }
            finally
            {
                c.Dispose();
                Conexion.Cierra(c);
            }


            x.Cells["B5:H" + rg].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:H" + rg].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:H" + rg].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:H" + rg].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:H" + rg].AutoFitColumns();

        }

        private void GeneraHojaCinco(ExcelWorksheet x, int anio, int mes, String distrito, String juzgado)
        {

            DateTime ini = new DateTime(anio, mes, 1);
            DateTime fin = ini.AddMonths(1).AddDays(-1);

            x.Cells["B1:F1"].Merge = true;
            x.Cells["B2:F2"].Merge = true;
            x.Cells["B5:F5"].Merge = true;

            x.Cells["B1"].Value = "Fuente de información: EXLAB";
            x.Cells["B2"].Value = "Periodo de información: " +
                (ini.ToString("dd \\de MMMM \\de yyyy", CultureInfo.CreateSpecificCulture("es-Mx")))
                + " al " + (fin.ToString("dd \\de MMMM \\de yyyy", CultureInfo.CreateSpecificCulture("es-Mx")));

            x.Cells["B5"].Value = "TASA DE CUMPLIMIENTO DE RESOLUCIONES";

            int rg = 6;

            x.Cells["B6"].Value = "AÑO";
            x.Cells["C6"].Value = "MES";
            x.Cells["D6"].Value = "JUZGADO";
            x.Cells["E6"].Value = "ID EXPEDIENTE";
            x.Cells["F6"].Value = "FECHA TERMINO";

            x.Cells["B5:F6"].Style.Font.Bold = true;
            x.Cells["B5:F6"].Style.Font.Color.SetColor(Color.White);
            x.Cells["B5:F6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            x.Cells["B5:F6"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#CC0000"));
            x.Cells["B5:F6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            try
            {
                c = Conexion.Conecta("dba");

                cmd = new MySqlCommand("select * from indicadores_pjem_laboral.tblresolucionesdetalle r " +
                    "inner join indicadores_pjem_laboral.tbljuzgados j on j.cvejuzgado = r.cvejuzgado " +
                    "where anio = @anioc and mes = @mesc and j.cvedistrito like @distritoc and j.cvejuzgado like @cvejuzgadoc ", c);
                cmd.Parameters.AddWithValue("@anioc", anio);
                cmd.Parameters.AddWithValue("@mesc", mes);
                cmd.Parameters.AddWithValue("@distritoc", "%" + distrito + "%");
                cmd.Parameters.AddWithValue("@cvejuzgadoc", "%" + juzgado + "%");

                MySqlDataReader r = cmd.ExecuteReader();

                while (r.Read())
                {
                    rg += 1;
                    x.Cells["B" + rg].Value = r.GetString("anio");
                    x.Cells["C" + rg].Value = mesinyear[r.GetInt16("mes") - 1];
                    x.Cells["D" + rg].Value = r.GetString("nomJuzgado");
                    x.Cells["E" + rg].Value = r.GetString("idExpediente");
                    x.Cells["F" + rg].Value = r.GetDateTime("fechaTermino").ToString("dd/MM/yyyy HH:mm:ss");

                }

            }
            catch (MySqlException e)
            {
                Debug.WriteLine("Hoja indicador resoluciones: " + e.Message);
            }
            finally
            {
                c.Dispose();
                Conexion.Cierra(c);
            }


            x.Cells["B5:F" + rg].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:F" + rg].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:F" + rg].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:F" + rg].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:F" + rg].AutoFitColumns();

        }
    
        private void GeneraHojaTres(ExcelWorksheet x, int anio, int mes, String distrito, String juzgado)
        {

            DateTime ini = new DateTime(anio, mes, 1);
            DateTime fin = ini.AddMonths(1).AddDays(-1);

            x.Cells["B1:J1"].Merge = true;
            x.Cells["B2:J2"].Merge = true;
            x.Cells["B5:J5"].Merge = true;

            x.Cells["B1"].Value = "Fuente de información: EXLAB";
            x.Cells["B2"].Value = "Periodo de información: " +
                (ini.ToString("dd \\de MMMM \\de yyyy", CultureInfo.CreateSpecificCulture("es-Mx")))
                + " al " + (fin.ToString("dd \\de MMMM \\de yyyy", CultureInfo.CreateSpecificCulture("es-Mx")));

            x.Cells["B5"].Value = "TIEMPO PARA LA PRACTICA DE NOTIFICACIONES PERSONALES";

            int rg = 6;

            x.Cells["B6"].Value = "AÑO";
            x.Cells["C6"].Value = "MES";
            x.Cells["D6"].Value = "JUZGADO";
            x.Cells["E6"].Value = "ID NOTIFICACION";
            x.Cells["F6"].Value = "ID ACUERDO";
            x.Cells["G6"].Value = "FECHA ACUERDO";
            x.Cells["H6"].Value = "FECHA NOTIFICACION";
            x.Cells["I6"].Value = "HORAS";
            x.Cells["J6"].Value = "NOMBRE JUZGADO";

            x.Cells["B5:J6"].Style.Font.Bold = true;
            x.Cells["B5:J6"].Style.Font.Color.SetColor(Color.White);
            x.Cells["B5:J6"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            x.Cells["B5:J6"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#CC0000"));
            x.Cells["B5:J6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            try
            {
                c = Conexion.Conecta("dba");

                cmd = new MySqlCommand("select * from indicadores_pjem_laboral.tblnotificacionesdetalle q " +
                    "inner join indicadores_pjem_laboral.tbljuzgados j on j.cvejuzgado = q.cvejuzgado " +
                  "where anio = @anioc and mes = @mesc and j.cvedistrito like @distritoc and j.cvejuzgado like @cvejuzgadoc ", c);
                cmd.Parameters.AddWithValue("@anioc", anio);
                cmd.Parameters.AddWithValue("@mesc", mes);
                cmd.Parameters.AddWithValue("@distritoc", "%" + distrito + "%");
                cmd.Parameters.AddWithValue("@cvejuzgadoc", "%" + juzgado + "%");

                MySqlDataReader r = cmd.ExecuteReader();

                while (r.Read())
                {
                    rg += 1;
                    x.Cells["B" + rg].Value = r.GetString("anio");
                    x.Cells["C" + rg].Value = mesinyear[r.GetInt16("mes") - 1];
                    x.Cells["D" + rg].Value = r.GetString("cveJuzgado");
                    x.Cells["E" + rg].Value = r.GetString("idNotificacion");
                    x.Cells["F" + rg].Value = r.GetString("idAcuerdo");
                    x.Cells["G" + rg].Value = r.GetDateTime("fechaAcuerdo").ToString("dd/MM/yyyy HH:mm:ss");
                    x.Cells["H" + rg].Value = r.GetDateTime("fechaNotificacion").ToString("dd/MM/yyyy HH:mm:ss");
                    x.Cells["I" + rg].Value = r.GetString("diferencia");
                    x.Cells["J" + rg].Value = r.GetString("nomJuzgado");

                }

            }
            catch (MySqlException e)
            {
                Debug.WriteLine("Hoja indicador notificaciones personales: " + e.Message);
            }
            finally
            {
                c.Dispose();
                Conexion.Cierra(c);
            }


            x.Cells["B5:J" + rg].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:J" + rg].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:J" + rg].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:J" + rg].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            x.Cells["B5:J" + rg].AutoFitColumns();

        }

    }
}