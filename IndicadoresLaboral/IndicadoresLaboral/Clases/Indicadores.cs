﻿using IndicadoresLaboral.Clases.Modelos;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace IndicadoresLaboral.Clases
{
    public class Indicadores
    {

        public static List<Indicador> TraeInformacion(int cveInd, String anio, String mes, String distrito, String juzgado)
        {
            List<Indicador> li = new List<Indicador>();
            MySqlConnection c = null;
            MySqlCommand cmd = null;
            string[] mesinyear = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };


            try
            {
                c = Conexion.Conecta("dba");
                if (anio.Equals("2020")) {
                    cmd = new MySqlCommand("TraeIndicador3", c);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("indicador", cveInd);
                    cmd.Parameters.AddWithValue("anioc", anio);
                    cmd.Parameters.AddWithValue("mesc", mes);
                    cmd.Parameters.AddWithValue("distrito", distrito);
                    cmd.Parameters.AddWithValue("juzgado", juzgado);
                }
                else {
                    //cmd = new MySqlCommand("TraeIndicador4", c);
                    cmd = new MySqlCommand("TraeIndicador5Correccion", c);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("indicador", cveInd);
                    cmd.Parameters.AddWithValue("anioc", anio);
                    cmd.Parameters.AddWithValue("mesc", mes);
                    cmd.Parameters.AddWithValue("distrito", distrito);
                    cmd.Parameters.AddWithValue("juzgado", juzgado);
                }
                MySqlDataReader r = cmd.ExecuteReader();

                while (r.Read())
                {
                    li.Add(new Indicador()
                    {
                        CveIndicador = r.GetInt32("cveindicador"),
                        Anio = r.GetString("anio"),
                        Mes = mesinyear[r.GetInt16("mes")-1],
                        Val1 = r.GetInt32("valor1"),
                        Val2 = r.GetInt32("valor2"),
                        Calculo = r.GetFloat("calculo")
                    });
                }

            }
            catch (MySqlException e)
            {
                Debug.WriteLine("Error en extraer informacion del indicador: " + e.Message);
            }
            finally
            {
                c.Dispose();
                Conexion.Cierra(c);
            }

            return li;
        }

    }
}