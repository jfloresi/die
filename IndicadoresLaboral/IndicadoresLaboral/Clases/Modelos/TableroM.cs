﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IndicadoresLaboral.Clases.Modelos
{
    [Serializable]
    public class TableroM
    {
        private string indicador;
        private string mes;
        private double calculo;
        private string estado;

        public string Indicador { get => indicador; set => indicador = value; }
        public string Mes { get => mes; set => mes = value; }
        public double Calculo { get => calculo; set => calculo = value; }
        public string Estado { get => estado; set => estado = value; }
    }
}