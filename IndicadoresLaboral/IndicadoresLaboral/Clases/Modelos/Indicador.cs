﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IndicadoresLaboral.Clases.Modelos
{
    [Serializable]
    public class Indicador
    {

        private int cveIndicador;
        private Int32 val1;
        private Int32 val2;
        private float calculo;
        private String anio;
        private String mes;

        public int CveIndicador { get => cveIndicador; set => cveIndicador = value; }
        public Int32 Val1 { get => val1; set => val1 = value; }
        public Int32 Val2 { get => val2; set => val2 = value; }
        public float Calculo { get => calculo; set => calculo = value; }
        public string Anio { get => anio; set => anio = value; }
        public string Mes { get => mes; set => mes = value; }
    }
}