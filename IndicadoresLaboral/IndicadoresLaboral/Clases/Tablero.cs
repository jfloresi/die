﻿using IndicadoresLaboral.Clases.Modelos;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace IndicadoresLaboral.Clases
{
    public class Tablero
    {
        public List<TableroM> LlenarTabla(string distrito,string anio,string juzgado)
        {
            
            try
            {
                if (juzgado == "0")
                {
                    juzgado = "";
                }

                if (distrito == "0")
                {
                    distrito = "";
                }

                int mes = 0;

                if (int.Parse(anio) < DateTime.Now.Year)
                {
                    mes = 12;
                }
                else
                {
                    mes = DateTime.Now.Month - 1;
                }


                List<TableroM> listaInd = new List<TableroM>();


                MySqlConnection c = Conexion.Conecta("dba");
                //MySqlCommand cmd = new MySqlCommand("TraerTablero", c);
               MySqlCommand cmd = new MySqlCommand("TraeTablero5Correccion", c);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("anioc", anio);
                cmd.Parameters.AddWithValue("mesc", mes);
                cmd.Parameters.AddWithValue("distrito", distrito);
                cmd.Parameters.AddWithValue("juzgado", juzgado);
                MySqlDataReader r = cmd.ExecuteReader();

                while (r.Read())
                {
                    TableroM t = new TableroM();
        
                    t.Indicador = r.GetString("cveIndicador");
                    t.Calculo = Math.Round(r.GetDouble("calculo"),2);
                    t.Mes = r.GetString("mes");

                    if (t.Indicador.Equals("1"))
                    {
                        if (t.Calculo <= 3.9)
                        {
                            t.Estado = "text-success";
                        }else if (t.Calculo >= 4 && t.Calculo <= 4.9)
                        {
                            t.Estado = "text-warning";
                        }else if (t.Calculo >= 5)
                        {
                            t.Estado = "text-danger";
                        }
                    }
                    else if (t.Indicador.Equals("2"))
                    {
                        if (t.Calculo <= 48)
                        {
                            t.Estado = "text-success";
                        }
                        else if (t.Calculo >= 48.1 && t.Calculo <= 71.9)
                        {
                            t.Estado = "text-warning";
                        }
                        else if (t.Calculo >= 72)
                        {
                            t.Estado = "text-danger";
                        }
                    }
                    else if (t.Indicador.Equals("3"))
                    {
                        if (t.Calculo <= 20)
                        {
                            t.Estado = "text-success";
                        }
                        else if (t.Calculo >= 20.1 && t.Calculo <= 23.99)
                        {
                            t.Estado = "text-warning";
                        }
                        else if (t.Calculo >= 24)
                        {
                            t.Estado = "text-danger";
                        }
                    }
                    else if (t.Indicador.Equals("4"))
                    {
                        if (t.Calculo >= 95)
                        {
                            t.Estado = "text-success";
                        }
                        //else if (t.Calculo >= 20.1 && t.Calculo <= 23.9)
                        //{
                        //    t.Estado = "amarillo";
                        //}
                        else if (t.Calculo < 95)
                        {
                            t.Estado = "text-danger";
                        }
                    }
                    else if (t.Indicador.Equals("5"))
                    {
                        if (t.Calculo >= 95)
                        {
                            t.Estado = "text-success";
                        }
                        //else if (t.Calculo >= 20.1 && t.Calculo <= 23.9)
                        //{
                        //    t.Estado = "amarillo";
                        //}
                        else if (t.Calculo < 95)
                        {
                            t.Estado = "text-danger";
                        }
                    }

                    listaInd.Add(t);

                }
                c.Dispose();
                c.Close();
                return listaInd;

            }
            catch (Exception e)
            {
                return null;
                Debug.WriteLine("No se establecio la conexion :" + e);
            }

        }

        public List<TableroM> LlenarTabla2(string distrito, string anio, string juzgado)
        {
            try
            {
                if (juzgado == "0")
                {
                    juzgado = "";
                }

                if (distrito == "0")
                {
                    distrito = "";
                }

                int mes = 0;

                if (int.Parse(anio) < DateTime.Now.Year)
                {
                    mes = 12;
                }
                else
                {
                    mes = DateTime.Now.Month - 1;
                }


                List<TableroM> listaInd = new List<TableroM>();


                MySqlConnection c = Conexion.Conecta("dba");
                MySqlCommand cmd = new MySqlCommand("TraerTablero2", c);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("anioc", anio);
                cmd.Parameters.AddWithValue("mesc", mes);
                cmd.Parameters.AddWithValue("distrito", distrito);
                cmd.Parameters.AddWithValue("juzgado", juzgado);
                MySqlDataReader r = cmd.ExecuteReader();

                while (r.Read())
                {
                    TableroM t = new TableroM();

                    t.Indicador = r.GetString("cveIndicador");
                    t.Calculo = Math.Round(r.GetDouble("calculo"), 2);
                    t.Mes = r.GetString("mes");

                    if (t.Indicador.Equals("1"))
                    {
                        if (t.Calculo <= 3.9)
                        {
                            t.Estado = "text-success";
                        }
                        else if (t.Calculo >= 4 && t.Calculo <= 4.9)
                        {
                            t.Estado = "text-warning";
                        }
                        else if (t.Calculo >= 5)
                        {
                            t.Estado = "text-danger";
                        }
                    }
                    else if (t.Indicador.Equals("2"))
                    {
                        if (t.Calculo <= 48)
                        {
                            t.Estado = "text-success";
                        }
                        else if (t.Calculo >= 48.1 && t.Calculo <= 71.9)
                        {
                            t.Estado = "text-warning";
                        }
                        else if (t.Calculo >= 72)
                        {
                            t.Estado = "text-danger";
                        }
                    }
                    else if (t.Indicador.Equals("3"))
                    {
                        if (t.Calculo <= 20)
                        {
                            t.Estado = "text-success";
                        }
                        else if (t.Calculo >= 20.1 && t.Calculo <= 23.99)
                        {
                            t.Estado = "text-warning";
                        }
                        else if (t.Calculo >= 24)
                        {
                            t.Estado = "text-danger";
                        }
                    }
                    else if (t.Indicador.Equals("4"))
                    {
                        if (t.Calculo >= 95)
                        {
                            t.Estado = "text-success";
                        }
                        //else if (t.Calculo >= 20.1 && t.Calculo <= 23.9)
                        //{
                        //    t.Estado = "amarillo";
                        //}
                        else if (t.Calculo < 95)
                        {
                            t.Estado = "text-danger";
                        }
                    }
                    else if (t.Indicador.Equals("5"))
                    {
                        if (t.Calculo >= 95)
                        {
                            t.Estado = "text-success";
                        }
                        //else if (t.Calculo >= 20.1 && t.Calculo <= 23.9)
                        //{
                        //    t.Estado = "amarillo";
                        //}
                        else if (t.Calculo < 95)
                        {
                            t.Estado = "text-danger";
                        }
                    }

                    listaInd.Add(t);

                }
                c.Dispose();
                c.Close();
                return listaInd;

            }
            catch (Exception e)
            {
                return null;
                Debug.WriteLine("No se establecio la conexion :" + e);
            }


        }

        public void ValidaRangos()
        {

        }
    }
}