﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Principal.aspx.cs" Inherits="IndicadoresLaboral.Laboral.Principal" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <%: Scripts.Render("~/Js") %>
    <%: Styles.Render("~/Css") %>
    <link rel="shortcut icon" type="image/x-icon" href="../Img/IconoPJEM.png" />
    <script src="https://kit.fontawesome.com/7fa5a81483.js" crossorigin="anonymous"></script>
    

    <title>Indicadores Laboral</title>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="script" runat="server" />

        <asp:Panel runat="server" ID="raiz" CssClass="position-fixed w-100 h-100">
            <asp:Panel runat="server" ID="cabecera" CssClass="cabecera row m-0 w-100 fondo_rojo">
                <asp:Image runat="server" ID="logo" ImageUrl="~/Img/pjem.png" Height="90%" />
                <asp:Label runat="server" CssClass="lead font-weight-light text-white text-center display-4 py-2 pl-2" Style="font-variant: small-caps; padding-left: 28.5rem!important;"> Indicadores Laboral</asp:Label>
            </asp:Panel>
            <asp:Panel runat="server" ID="cuerpo" CssClass="position-absolute row m-0 w-100">
                <asp:Panel runat="server" ID="menu" CssClass="m-0 h-100 fondo_rojo">

                    <asp:Panel runat="server" ID="combos" CssClass="p-1">
                        <asp:Panel runat="server" CssClass="form-group m-0">
                            <asp:Label runat="server" AssociatedControlID="distritos" Font-Size="10px" CssClass="text-white font-weight-bolder text-uppercase">
                                Distrito
                            </asp:Label>
                            <asp:DropDownList runat="server" ID="distritos" Font-Size="12px" CssClass="form-control custom-select" OnSelectedIndexChanged="CargaJuzgados" AppendDataBoundItems="true" AutoPostBack="true">
                             <asp:ListItem Value="0" Text="Todos los Distritos" Selected="True" />
                                </asp:DropDownList>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="form-group m-0">
                            <asp:Label runat="server" AssociatedControlID="juzgados" Font-Size="10px" CssClass="text-white font-weight-bolder text-uppercase">
                                Tribunales
                            </asp:Label>
                            <asp:DropDownList runat="server" ID="juzgados" Font-Size="12px" CssClass="form-control custom-select" AppendDataBoundItems="true">
                                <asp:ListItem Value="0" Text="Todos los Tribunales" Selected="True" />
                            </asp:DropDownList>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="form-group m-0">
                            <asp:Label runat="server" AssociatedControlID="anios" Font-Size="10px" CssClass="text-white font-weight-bolder text-uppercase">
                                Año
                            </asp:Label>
                            <asp:DropDownList runat="server" ID="anios" Font-Size="12px" CssClass="form-control custom-select" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="CambiaMes"/>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="form-group m-0">
                            <asp:Label runat="server" AssociatedControlID="meses" Font-Size="10px" CssClass="text-white font-weight-bolder text-uppercase">
                                Mes
                            </asp:Label>
                            <asp:DropDownList runat="server" ID="meses" Font-Size="12px" CssClass="form-control custom-select"  AppendDataBoundItems="true" AutoPostBack="true"/>
                        </asp:Panel>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="menu_indicadores" CssClass="list-group p-0">
                        <asp:Panel runat="server" CssClass="list-group-item list-group-item-action dropdown dropright p-0">
                            <asp:LinkButton runat="server" CssClass="dropdown-item d-flex flex-wrap align-content-center" Height="48.7px" ID="link_indicadores" data-toggle="dropdown">
                                INDICADORES
                            </asp:LinkButton>
                            <asp:Panel runat="server" CssClass=" dropdown-menu m-0 submenu">
                                <asp:LinkButton runat="server" ID="ind1" OnClick="GeneraIndicador" CssClass="dropdown-item list-group-item-action">
                                    Quejas del servicio de atención al público
                                </asp:LinkButton>
                                <asp:Panel runat="server" CssClass="dropdown-divider" />
                                <asp:LinkButton runat="server" ID="ind2" OnClick="GeneraIndicador" CssClass="dropdown-item list-group-item-action">
                                    Tiempo para el acuerdo de actuaciones
                                </asp:LinkButton>
                                <asp:Panel runat="server" CssClass="dropdown-divider" />
                                <asp:LinkButton runat="server" ID="ind3" OnClick="GeneraIndicador" CssClass="dropdown-item list-group-item-action">
                                    Tiempo para la práctica de notificaciones personales
                                </asp:LinkButton>
                                <asp:Panel runat="server" CssClass="dropdown-divider" />
                                <asp:LinkButton runat="server" ID="ind4" OnClick="GeneraIndicador" CssClass="dropdown-item list-group-item-action">
                                    Porcentaje de audiencias celebradas
                                </asp:LinkButton>
                                <asp:Panel runat="server" CssClass="dropdown-divider" />
                                <asp:LinkButton runat="server" ID="ind5" OnClick="GeneraIndicador" CssClass="dropdown-item list-group-item-action">
                                    Tasa de cumplimiento de resoluciones
                                </asp:LinkButton>
                                <%--<asp:Panel runat="server" CssClass="dropdown-divider" />--%>
                                <%--<asp:LinkButton runat="server" CssClass="dropdown-item d-flex flex-wrap align-content-center" Height="48.7px" ID="LinkOtros_indicadores" data-toggle="dropdown">
                                OTROS INDICADORES
                            </asp:LinkButton>--%>
                            <%--<asp:Panel runat="server" CssClass=" dropdown-menu m-0 submenu">
                                <asp:LinkButton runat="server" ID="indotro1" OnClick="GeneraIndicador" CssClass="dropdown-item list-group-item-action">
                                    Indicador1
                                </asp:LinkButton>
                                <asp:Panel runat="server" CssClass="dropdown-divider" />
                                <asp:LinkButton runat="server" ID="indotro2" OnClick="GeneraIndicador" CssClass="dropdown-item list-group-item-action">
                                    Indicador2
                                </asp:LinkButton>
                                <asp:Panel runat="server" CssClass="dropdown-divider" />
                                <asp:LinkButton runat="server" ID="indotro3" OnClick="GeneraIndicador" CssClass="dropdown-item list-group-item-action">
                                    Indicador3
                                </asp:LinkButton>
                                <asp:Panel runat="server" CssClass="dropdown-divider" />
                                <asp:LinkButton runat="server" ID="indotro4" OnClick="GeneraIndicador" CssClass="dropdown-item list-group-item-action">
                                    Indicador4
                                </asp:LinkButton>
                                <asp:Panel runat="server" CssClass="dropdown-divider" />
                                <asp:LinkButton runat="server" ID="indotro5" OnClick="GeneraIndicador" CssClass="dropdown-item list-group-item-action">
                                    Indicador5
                                </asp:LinkButton>
                            </asp:Panel>--%>
                            </asp:Panel>                       
                        </asp:Panel>
                        <asp:LinkButton runat="server" CssClass="list-group-item list-group-item-action" ID="link_tablero" OnClick="ParametrosTablero">
                            TABLERO   
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" CssClass="list-group-item list-group-item-action" ID="link_general" OnClick="GeneraIndicadores">
                            INFORMACIÓN GENERAL
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" CssClass="list-group-item list-group-item-action" ID="link_detalle" OnClick="GeneraDetalle">
                            DETALLE INIDICADORES
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" CssClass="list-group-item list-group-item-action" ID="link_cerrarsesion" OnClick="CierraSesion">
                            CERRAR SESIÓN
                        </asp:LinkButton>
                    </asp:Panel>

                </asp:Panel>
                <asp:Panel runat="server" ID="contenido" CssClass="m-0 h-100">
                    <asp:Panel runat="server" ID="indicadores" Visible="false" CssClass="w-100 h-100">
                        <asp:Panel runat="server" ID="panel_info" CssClass="row p-0 m-0">
                            <asp:Label runat="server" ID="indicador" CssClass="col-xl-4 p-3" />
                             <asp:Label runat="server" ID="Label1" CssClass="col-xl-4 p-3" />
                            <asp:Panel runat="server" ID="formula" CssClass="col-xl-4 p-3">
                                <%--<asp:Table ID="tabla_formula"
                                    GridLines="Both"
                                    HorizontalAlign="Center"
                                    Font-Names="Verdana"
                                    Font-Size="8pt"
                                    CellPadding="15"
                                    CellSpacing="0"
                                    runat="server" />--%>
                                
                                 <table style="width: 351px;"><tr>
                                    <td><asp:Label runat="server" ID="arriba" CssClass="col-xl-5 p-3" /></td></tr>
                                    <tr><td><hr style="height: 0px;background-color: black;"/></td>
                                    <td><asp:Label runat="server" ID="por" CssClass="col-xl-5 p-3" /></td></tr>
                                    <tr><td><asp:Label runat="server" ID="abajo" CssClass="col-xl-5 p-3" /></td></tr>
                                    </table>
                                    
                            </asp:Panel>
                            <asp:Label runat="server" ID="distritojugado" CssClass="col-xl-12 p-3 m-0" Style="font-weight: bold;" />
                            <asp:Panel runat="server" ID="datos" CssClass="col-xl-12 row p-2 m-0">
                                <asp:Panel runat="server" CssClass="col-xl-4 px-2 m-0">
                                    <asp:Panel runat="server" CssClass="p-3 row m-0" Style="background-color: #487db0;">
                                        <asp:Panel runat="server" CssClass="col-md-2 my-3">
                                            <i class="fa fa-bars fa-3x" aria-hidden="true" style="color: white;"></i>
                                        </asp:Panel>
                                        <asp:Panel runat="server" CssClass="col-md-10 row p-0 m-0" Style="color: white;">
                                            <asp:Label runat="server" ID="valor1" CssClass="col-xl-12" />
                                            <asp:Label runat="server" ID="valor1info" CssClass="col-xl-12" />
                                        </asp:Panel>
                                    </asp:Panel>
                                </asp:Panel>
                                <asp:Panel runat="server" CssClass="col-xl-4 px-2 m-0">
                                    <asp:Panel runat="server" CssClass="p-3 row m-0" Style="background-color: #65baaf;">
                                        <asp:Panel runat="server" CssClass="col-md-2 my-3">
                                            <i class="fa fa-bars fa-3x" aria-hidden="true" style="color: white;"></i>
                                        </asp:Panel>
                                        <asp:Panel runat="server" CssClass="col-md-10 row p-0 m-0" Style="color: white;">
                                            <asp:Label runat="server" ID="valor2" CssClass="col-xl-12" />
                                            <asp:Label runat="server" ID="valor2info" CssClass="col-xl-12" />
                                        </asp:Panel>
                                    </asp:Panel>
                                </asp:Panel>
                                <%--<asp:Panel runat="server" CssClass="col-xl-3 px-2 m-0">
                                    <asp:Panel runat="server" CssClass="p-3 row m-0" Style="background-color: #ffb259;">
                                        <asp:Panel runat="server" CssClass="col-md-2 my-3">
                                            <i class="fa fa-bars fa-3x" aria-hidden="true" style="color: white;"></i>
                                        </asp:Panel>
                                        <asp:Panel runat="server" CssClass="col-md-10 row" Style="color: white;">
                                            <table><tr>
                                    <td><asp:Label runat="server" ID="arriba" CssClass="col-xl-5 p-3" /></td></tr>
                                    <tr><td><hr style="height: 0px;background-color: white;"/></td>
                                    <td><asp:Label runat="server" ID="por" CssClass="col-xl-5 p-3" /></td></tr>
                                    <tr><td><asp:Label runat="server" ID="abajo" CssClass="col-xl-5 p-3" /></td></tr>
                                    </table>
                                        </asp:Panel>
                                    </asp:Panel>
                                </asp:Panel>--%>
                                <asp:Panel runat="server" CssClass="col-xl-4 px-2 m-0">
                                    <asp:Panel runat="server" CssClass="p-3 row m-0" Style="background-color: #ffb259;">
                                        <asp:Panel runat="server" CssClass="col-md-2 my-3">
                                           <i class="fa fa-bars fa-3x" aria-hidden="true" style="color: white;"></i>
                                        </asp:Panel>
                                        <asp:Panel runat="server" CssClass="col-md-10 row" Style="color: white;">
                                            <asp:Label runat="server" ID="calculo" CssClass="col-xl-12" />
                                            <asp:Label runat="server" ID="calculoinfo" CssClass="col-xl-12" />
                                        </asp:Panel>
                                    </asp:Panel>
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="panel_graficas" CssClass="row m-0 p-2">
                            <asp:Panel runat="server" ID="lineas" CssClass="col-xl-6 p-2">
                                <asp:Panel runat="server" CssClass="card">
                                    <asp:Label runat="server" CssClass="card-header">Estadistica del año</asp:Label>
                                    <canvas id="lineas_chart" class="card-body m-0"></canvas>
                                </asp:Panel>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pastel" CssClass="col-xl-6 p-2">
                                <asp:Panel runat="server" CssClass="card">
                                    <asp:Label runat="server" CssClass="card-header">Promedio del mes</asp:Label>
                                    <canvas id="pastel_chart" class="card-body p-0 m-0"></canvas>
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="tablero" Visible="false" CssClass="position-absolute h-100 px-2">
                        <br />
                        <asp:Label runat="server" ID="distritoNombre" CssClass="col-xl-5 p-3" Style="font-weight: bold;"></asp:Label>
                        <br />
                        <asp:Label runat="server" ID="juzgadoNom" CssClass="col-xl-5 p-3" Style="font-weight: bold;"></asp:Label>
                        <br />
                        <asp:Label runat="server" ID="anioTablero" CssClass="col-xl-5 p-3" Style="font-weight: bold;"></asp:Label>
                        <table class="table table-bordered" style="width: 70%; background-color: white;font-size: 13px;">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Proceso</th>
                                    <th>Indicador</th>
                                    <th>Unidad</th>
                                    <th>Meta</th>
                                    <th>Periodicidad</th>
                                    <th>Bueno</th>
                                    <th>Regular</th>
                                    <th>Malo</th>
                                    <th>Enero</th>
                                    <th>Febrero</th>
                                    <th>Marzo</th>
                                    <th>Abril</th>
                                    <th>Mayo</th>
                                    <th>Junio</th>
                                    <th>Julio</th>
                                    <th>Agosto</th>
                                    <th>Septiembre</th>
                                    <th>Octubre</th>
                                    <th>Noviembre</th>
                                    <th>Diciembre</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="success">
                                    <td>1</td>
                                    <td>Atención al Público</td>
                                    <td>Quejas del servicio de atención al público</td>
                                    <td>%</td>
                                    <td>5% o menos</td>
                                    <td>Mensual</td>
                                    <td>
                                        <p class="text-success">0%-3.9%</p>
                                    </td>
                                    <td>
                                        <p class="text-warning">4%-4.9%</p>
                                    </td>
                                    <td>
                                        <p class="text-danger">5%-100%</p>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="eneInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="febInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="marInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="abrInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="mayInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="junInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="julInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="agoInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="sepInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="octInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="novInd1"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="dicInd1"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Trámite Judicial</td>
                                    <td>Tiempo para el acuerdo de actuaciones</td>
                                    <td>Horas</td>
                                    <td>72 Horas laborales o menos</td>
                                    <td>Mensual</td>
                                    <td>
                                        <p class="text-success">0h-48h</p>
                                    </td>
                                    <td>
                                        <p class="text-warning">48.1h-71.9h</p>
                                    </td>
                                    <td>
                                        <p class="text-danger">72hr o más</p>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="eneInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="febInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="marInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="abrInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="mayInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="junInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="julInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="agoInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="sepInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="octInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="novInd2"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="dicInd2"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Trámite Judicial</td>
                                    <td>Tiempo para la práctica de notificaciones personales</td>
                                    <td>Horas</td>
                                    <td>24 horas laborales o menos</td>
                                    <td>Mensual</td>
                                    <td>
                                        <p class="text-success">0h–20h</p>
                                    </td>
                                    <td>
                                        <p class="text-warning">20.1h–23.9h</p>
                                    </td>
                                    <td>
                                        <p class="text-danger">24hr o más</p>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="eneInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="febInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="marInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="abrInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="mayInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="junInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="julInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="agoInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="sepInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="octInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="novInd3"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="dicInd3"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Audiencia de Oralidad</td>
                                    <td>Porcentaje de audiencias celebradas</td>
                                    <td>%</td>
                                    <td>95% o más</td>
                                    <td>Mensual</td>
                                    <td>
                                        <p class="text-success">95% o más</p>
                                    </td>
                                    <td>
                                        <p class="text-warning">/</p>
                                    </td>
                                    <td>
                                        <p class="text-danger">/</p>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="eneInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="febInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="marInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="abrInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="mayInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="junInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="julInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="agoInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="sepInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="octInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="novInd4"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="dicInd4"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Cumplimiento de Resoluciones</td>
                                    <td>Tasa de cumplimiento de resoluciones</td>
                                    <td>%</td>
                                    <td>95% o más</td>
                                    <td>Mensual</td>
                                    <td>
                                        <p class="text-success">95% o más</p>
                                    </td>
                                    <td>
                                        <p class="text-warning">/</p>
                                    </td>
                                    <td>
                                        <p class="text-danger">/</p>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="eneInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="febInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="marInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="abrInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="mayInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="junInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="julInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="agoInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="sepInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="octInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="novInd5"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="dicInd5"></asp:Label></td>
                                </tr>
                                <%--<tr>
                                    <td>6</td>
                                    <td>Nuevo Indicador</td>
                                    <td>Tasa de cumplimiento de resoluciones</td>
                                    <td>%</td>
                                    <td>95% o más</td>
                                    <td>Mensual</td>
                                    <td>
                                        <p class="text-success">95% o más</p>
                                    </td>
                                    <td>
                                        <p class="text-warning">/</p>
                                    </td>
                                    <td>
                                        <p class="text-danger">/</p>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="eneInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="febInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="marInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="abrInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="mayInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="junInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="julInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="agoInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="sepInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="octInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="novInd6"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="dicInd6"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>Nuevo Indicador</td>
                                    <td>Tasa de cumplimiento de resoluciones</td>
                                    <td>%</td>
                                    <td>95% o más</td>
                                    <td>Mensual</td>
                                    <td>
                                        <p class="text-success">95% o más</p>
                                    </td>
                                    <td>
                                        <p class="text-warning">/</p>
                                    </td>
                                    <td>
                                        <p class="text-danger">/</p>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="eneInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="febInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="marInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="abrInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="mayInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="junInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="julInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="agoInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="sepInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="octInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="novInd7"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="dicInd7"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Nuevo Indicador</td>
                                    <td>Tasa de cumplimiento de resoluciones</td>
                                    <td>%</td>
                                    <td>95% o más</td>
                                    <td>Mensual</td>
                                    <td>
                                        <p class="text-success">95% o más</p>
                                    </td>
                                    <td>
                                        <p class="text-warning">/</p>
                                    </td>
                                    <td>
                                        <p class="text-danger">/</p>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="eneInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="febInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="marInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="abrInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="mayInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="junInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="julInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="agoInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="sepInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="octInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="novInd8"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="dicInd8"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Nuevo Indicador</td>
                                    <td>Tasa de cumplimiento de resoluciones</td>
                                    <td>%</td>
                                    <td>95% o más</td>
                                    <td>Mensual</td>
                                    <td>
                                        <p class="text-success">95% o más</p>
                                    </td>
                                    <td>
                                        <p class="text-warning">/</p>
                                    </td>
                                    <td>
                                        <p class="text-danger">/</p>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="eneInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="febInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="marInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="abrInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="mayInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="junInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="julInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="agoInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="sepInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="octInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="novInd9"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="dicInd9"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>Nuevo Indicador</td>
                                    <td>Tasa de cumplimiento de resoluciones</td>
                                    <td>%</td>
                                    <td>95% o más</td>
                                    <td>Mensual</td>
                                    <td>
                                        <p class="text-success">95% o más</p>
                                    </td>
                                    <td>
                                        <p class="text-warning">/</p>
                                    </td>
                                    <td>
                                        <p class="text-danger">/</p>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="eneInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="febInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="marInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="abrInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="mayInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="junInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="julInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="agoInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="sepInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="octInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="novInd10"></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" ID="dicInd10"></asp:Label></td>
                                </tr>--%>
                            </tbody>
                        </table>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="general" Visible="false" CssClass="row h-100 m-0 p-2">                          
                        <asp:Panel runat="server" CssClass="col-xl-12 row p-0 m-0">
                            <%--<asp:Label runat="server" CssClass="col-xl-12 p-0 m-0">
                                
                            </asp:Label>--%>
                             <asp:Label runat="server" ID="distrito_ge"  CssClass="col-xl-12 p-1 m-0" Style="font-weight: bold;font-size: 13.5px;"/>
                            <br />
                            <asp:Label runat="server" ID="nom_ge"  CssClass="col-xl-12 p-1 m-0" Style="font-weight: bold;font-size: 13.5px;"/>
                            <br />
                            <asp:Label runat="server" ID="anio_ge"  CssClass="col-xl-12 p-1 m-0" Style="font-weight: bold;font-size: 13.5px;"/>

                        </asp:Panel>
                        <!-- indicadores 1----------------------->
                        <asp:Panel runat="server" ID="Panel1" CssClass="col-xl-4 row p-1 m-0">
                            <canvas id="lineas_ind1" class="m-0 col-md-12 p-1 bg-white"></canvas>
                            <asp:Panel runat="server" CssClass="col-md-12 row p-0 m-0" Style="color: black;">
                                <table border ="1" style="width:100%;font-size: 13px;">   
                                <tr><td><asp:Label runat="server" ID="valor1ind1" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="valor2ind1" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="calculoind1" CssClass="col-xl-12 m-0 p-0" /></td> </tr> 
                                </table>
                            </asp:Panel>
                        </asp:Panel>
                        <!-- indicadores 2----------------------->
                        <asp:Panel runat="server" ID="Panel2" CssClass="col-xl-4 row p-1 m-0">
                            <canvas id="lineas_ind2" class="m-0 col-md-12 p-1 bg-white"></canvas>
                            <asp:Panel runat="server" CssClass="col-md-12 row p-0 m-0" Style="color: black;">
                                <table border ="1" style="width:100%;font-size: 13px;">
                                <tr><td><asp:Label runat="server" ID="valor1ind2" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="valor2ind2" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="calculoind2" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                </table>
                            </asp:Panel>
                        </asp:Panel>
                       
                        <!-- indicadores 3----------------------->
                        <asp:Panel runat="server" ID="Panel3" CssClass="col-xl-4 row p-1 m-0">
                            <canvas id="lineas_ind3" class="m-0 col-md-12 p-1 bg-white"></canvas>
                            <asp:Panel runat="server" CssClass="col-md-12 row p-0 m-0" Style="color: black;">
                                <table border ="1" style="width:100%;font-size: 13px;">
                                <tr><td><asp:Label runat="server" ID="valor1ind3" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="valor2ind3" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="calculoind3" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                </table>
                            </asp:Panel>
                        </asp:Panel>
                        
                        <!-- indicadores 4----------------------->
                        <asp:Panel runat="server" ID="Panel4" CssClass="col-xl-4 row p-1 m-0">
                            <canvas id="lineas_ind4" class="m-0 col-md-12 p-1 bg-white"></canvas>
                            <asp:Panel runat="server" CssClass="col-md-12 row p-0 m-0" Style="color: black;">
                                <table border ="1" style="width:100%;font-size: 13px;">
                                <tr><td><asp:Label runat="server" ID="valor1ind4" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="valor2ind4" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="calculoind4" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                    </table>
                            </asp:Panel>
                        </asp:Panel>

                        <!-- indicadores 5----------------------->
                        <asp:Panel runat="server" ID="Panel5" CssClass="col-xl-4 row p-1 m-0">
                            <canvas id="lineas_ind5" class="m-0 col-md-12 p-1 bg-white"></canvas>
                            <asp:Panel runat="server" CssClass="col-md-12 row p-0 m-0" Style="color: black;">
                                <table border ="1" style="width:100%;font-size: 13px;">
                                <tr><td><asp:Label runat="server" ID="valor1ind5" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="valor2ind5" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="calculoind5" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                    </table>
                            </asp:Panel>
                        </asp:Panel>
                        <!-- indicadores 6----------------------->
                        <%--<asp:Panel runat="server" ID="Panel6" CssClass="col-xl-4 row p-1 m-0">
                            <canvas id="lineas_ind6" class="m-0 col-md-12 p-1 bg-white"></canvas>
                            <asp:Panel runat="server" CssClass="col-md-12 row p-0 m-0" Style="color: black;">
                                <table border ="1" style="width:100%;font-size: 13px;">
                                <tr><td><asp:Label runat="server" ID="valor1ind6" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="valor2ind6" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="calculoind6" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                    </table>
                            </asp:Panel>
                        </asp:Panel> --%>
                        <!-- indicadores 7----------------------->
                       <%-- <asp:Panel runat="server" ID="Panel7" CssClass="col-xl-4 row p-1 m-0">
                            <canvas id="lineas_ind7" class="m-0 col-md-12 p-1 bg-white"></canvas>
                            <asp:Panel runat="server" CssClass="col-md-12 row p-0 m-0" Style="color: black;">
                                <table border ="1" style="width:100%;font-size: 13px;">
                                <tr><td><asp:Label runat="server" ID="valor1ind7" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="valor2ind7" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="calculoind7" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                    </table>
                            </asp:Panel>
                        </asp:Panel> --%>
                        <!-- indicadores 8----------------------->
                        <%--<asp:Panel runat="server" ID="Panel8" CssClass="col-xl-4 row p-1 m-0">
                            <canvas id="lineas_ind8" class="m-0 col-md-12 p-1 bg-white"></canvas>
                            <asp:Panel runat="server" CssClass="col-md-12 row p-0 m-0" Style="color: black;">
                                <table border ="1" style="width:100%;font-size: 13px;">
                                <tr><td><asp:Label runat="server" ID="valor1ind8" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="valor2ind8" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="calculoind8" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                    </table>
                            </asp:Panel>
                        </asp:Panel> --%>
                        <!-- indicadores 9----------------------->
                        <%--<asp:Panel runat="server" ID="Panel9" CssClass="col-xl-4 row p-1 m-0">
                            <canvas id="lineas_ind9" class="m-0 col-md-12 p-1 bg-white"></canvas>
                            <asp:Panel runat="server" CssClass="col-md-12 row p-0 m-0" Style="color: black;">
                                <table border ="1" style="width:100%;font-size: 13px;">
                                <tr><td><asp:Label runat="server" ID="valor1ind9" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="valor2ind9" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="calculoind9" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                    </table>
                            </asp:Panel>
                        </asp:Panel> --%>
                        <!-- indicadores 10----------------------->
                        <%--<asp:Panel runat="server" ID="Panel10" CssClass="col-xl-4 row p-1 m-0">
                            <canvas id="lineas_ind10" class="m-0 col-md-12 p-1 bg-white"></canvas>
                            <asp:Panel runat="server" CssClass="col-md-12 row p-0 m-0" Style="color: black;">
                                <table border ="1" style="width:100%;font-size: 13px;">
                                <tr><td><asp:Label runat="server" ID="valor1ind10" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="valor2ind10" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                <tr><td><asp:Label runat="server" ID="calculoind10" CssClass="col-xl-12 m-0 p-0" /></td> </tr>
                                    </table>
                            </asp:Panel>
                        </asp:Panel> --%>
                    </asp:Panel>

                </asp:Panel>
            </asp:Panel>
        </asp:Panel>

        <asp:UpdateProgress runat="server" ID="progressPanel" runAssociatedUpdatePanelID="update_panel">
            <ProgressTemplate>
                <asp:Panel runat="server" ID="mascara" Style="font-size: 50vh"
                    CssClass="position-fixed w-100 h-100 d-flex justify-content-center align-items-center">
                    <i class="fas fa-spinner fa-pulse" style="color: white"></i>
                </asp:Panel>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </form>
</body>
</html>
