﻿function exporta() {
    setTimeout(function () {
        html2canvas(document.querySelector("#general")).then(canvas => {
            var imgData = canvas.toDataURL('image/png');
            var doc = new jsPDF('p', 'mm', 'letter');
            doc.setFontSize(20);
            doc.text(15, 25, "INDICADORES GENERALES");
            doc.addImage(imgData, 'PNG', 5, 35, 195, 110); // 1: canvas, 2: formato, 3: posicion x, 4: posicion: y, 5: ancho, 6: largo
            doc.save("Indicadores.pdf")}, 5000);
    });

}
