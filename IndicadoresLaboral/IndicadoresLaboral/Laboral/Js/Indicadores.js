﻿function Lineas(datos, titulo, tituloLinea, yTitulo) {

    var meses = [];
    var totales = [];

    for (var i in datos) {
        meses.push(datos[i].Mes);
        totales.push(datos[i].Calculo);
    }

    var config = {
        type: 'line',
        data: {
            labels: meses,
            datasets: [{
                label: tituloLinea,
                backgroundColor: "#ea6852",
                borderColor: "#ea6852",
                data: totales,
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: titulo
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "Meses"
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: yTitulo
                    }
                }]
            }
        }
    };
 
    var ctx = document.getElementById('lineas_chart').getContext('2d');
    window.myLine = new Chart(ctx, config);
}

function Pastel(titulo, titulos, calculos, colores) {

    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: calculos,
                backgroundColor: colores,
                label: 'Dataset 1'
            }],
            labels: titulos
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: titulo
            }
        }
    };


    var ctx = document.getElementById('pastel_chart').getContext('2d');
    window.myPie = new Chart(ctx, config);

}

function LineasGeneral(datos, titulo, tituloLinea, yTitulo, id) {

    var meses = [];
    var totales = [];

    for (var i in datos) {
        meses.push(datos[i].Mes);
        totales.push(datos[i].Calculo);
    }

    var config = {
        type: 'line',
        data: {
            labels: meses,
            datasets: [{
                label: tituloLinea,
                backgroundColor: "#487db0",
                borderColor: "#487db0",
                data: totales,
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: titulo
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: "Meses"
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: yTitulo
                    }
                }]
            }
        }
    };

    var ctx = document.getElementById(id).getContext('2d');
    window.myLine = new Chart(ctx, config);
}		
