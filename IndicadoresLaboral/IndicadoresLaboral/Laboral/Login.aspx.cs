﻿using IndicadoresLaboral.Clases;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IndicadoresLaboral.Laboral
{
    public partial class Login : System.Web.UI.Page
    {
     

        protected void Page_Load(object sender, EventArgs e)
        {

            Response.AppendHeader("Cache-Control", "no-store");
            Verifica();
        }


        private void Verifica()
        {
            String urlPeticion = Page.Request.UrlReferrer.AbsoluteUri;
            Uri url = new Uri(Page.Request.Url.AbsoluteUri);
            String usuario = HttpUtility.ParseQueryString(url.Query).Get("usr");

            if (urlPeticion.Contains("gestionjudicial.pjedomex.gob.mx") && usuario != null)
            {

                Byte[] data = Convert.FromBase64String(usuario);
                String usr = ASCIIEncoding.ASCII.GetString(data);
                MySqlConnection c = null;
                MySqlCommand cmd = null;
                String query = "SELECT * FROM htsj_spacusatorio_laboral.users " +
                               "WHERE email = @usr and activated = 1; ";

                try
                {
                    c = Conexion.Conecta("Naxit");
                    cmd = new MySqlCommand(query, c);
                    cmd.Parameters.AddWithValue("usr", usr);
                    MySqlDataReader r = cmd.ExecuteReader();
                    if (r.HasRows)
                    {
                        List<String> acceso = new List<string>();
                        acceso.Add(usuario);
                        acceso.Add(urlPeticion);
                        Session["acceso"] = acceso;
                        Response.Redirect("Laboral/Principal.aspx");
                        //Response.Redirect("~/");
                    }
                    else
                    {
                         Response.Redirect("http://gestionjudicial.pjedomex.gob.mx/ideas/laboral/proceso/atencion-al-publico");
                    }
                }
                catch (MySqlException e)
                {
                    Debug.WriteLine("Error en verificar usuario: " + e.Message);
                    Response.Redirect("http://gestionjudicial.pjedomex.gob.mx/ideas/laboral/proceso/atencion-al-publico");
                }
                finally
                {
                    c.Dispose();
                    Conexion.Cierra(c);
                }
            }
            else
            {
              Response.Redirect("http://gestionjudicial.pjedomex.gob.mx/ideas/laboral/proceso/atencion-al-publico");
            }
        }
    }
}