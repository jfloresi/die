﻿using IndicadoresLaboral.Clases;
using IndicadoresLaboral.Clases.Modelos;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ListItem = System.Web.UI.WebControls.ListItem;

namespace IndicadoresLaboral.Laboral
{
    public partial class Principal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["acceso"] == null)
                {
                    //Response.Redirect("http://gestionjudicial.pjedomex.gob.mx/ideas/laboral/proceso/tramite-judicial");
                }
                CargaCombos();

            }
        }

        private void CargaCombos()
        {
            MySqlConnection c = null;
            MySqlCommand cmd = null;
            String query;

            try
            {

                c = Conexion.Conecta("dba");

                /*DISTRITO*/

                query = "select cveDistrito, nomDistrito from indicadores_pjem_laboral.tbldistritos where cvedistrito in(3,7,8,14,15) order by cveDistrito";
                cmd = new MySqlCommand(query, c);
                distritos.DataSource = cmd.ExecuteReader();
                distritos.DataTextField = "nomDistrito";
                distritos.DataValueField = "cveDistrito";
                distritos.DataBind();
                //distritos.SelectedValue = "7";
                c.Dispose();

                //c = Conexion.Conecta("dba");

                ///*REGION*/

                //query = "select cveJuzgado, desJuzgado from indicadores_pjem_laboral.tbljuzgados " +
                //    "where cveDistrito = "+distritos.SelectedValue+" order by cveJuzgado";
                //cmd = new MySqlCommand(query, c);
                //juzgados.DataSource = cmd.ExecuteReader();
                //juzgados.DataTextField = "desJuzgado";
                //juzgados.DataValueField = "cveJuzgado";
                //juzgados.DataBind();

                //c.Dispose();
            }
            catch (MySqlException ex)
            {
                Debug.WriteLine("Error al cargar combos distritos y juzgados");
            }
            finally
            {
                c.Dispose();
                Conexion.Cierra(c);
            }


            string[] mesinyear = {"", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
            int minmes = 1;
            int mesMax = (DateTime.Now.Month == 1) ? 12 : (DateTime.Now.Month - 1);
            int anioMax = (DateTime.Now.Month == 1) ? (DateTime.Now.Year - 1) : DateTime.Now.Year;


            for (int i = 2020; i <= anioMax; i++)
            {
                System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem();
                li.Text = i.ToString();
                li.Value = i.ToString();
                anios.Items.Add(li);
            }

            if (DateTime.Now.Year == 2020)
            {
                minmes = 11;
            }



            for (int mes = minmes; mes <= mesMax; mes++)
            {
                ListItem l = new ListItem();
                l.Text = mesinyear[mes];
                l.Value = mes.ToString();
                meses.Items.Add(l);
            }

            anios.SelectedValue = anioMax.ToString();
            meses.SelectedValue = mesMax.ToString();
        }

        public void CargaJuzgados(object sender, EventArgs e)
        {
            indicadores.Visible = false;
            tablero.Visible = false;
            general.Visible = false;
            MySqlConnection c = null;
            MySqlCommand cmd = null;
            String query;

            juzgados.Items.Clear();
            juzgados.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Todos los Tribunales", ""));
            try
            {
                c = Conexion.Conecta("dba");
                query = "select cveJuzgado, desJuzgado from indicadores_pjem_laboral.tbljuzgados " +
                    "where cveDistrito = " + distritos.SelectedValue + " order by cveJuzgado";
                cmd = new MySqlCommand(query, c);
                juzgados.DataSource = cmd.ExecuteReader();
                juzgados.DataTextField = "desJuzgado";
                juzgados.DataValueField = "cveJuzgado";
                juzgados.DataBind();

                c.Dispose();
            }
            catch (MySqlException ex)
            {
                Debug.WriteLine("Error al cargar combos distritos y juzgados");
            }
            finally
            {
                c.Dispose();
                Conexion.Cierra(c);
            }
        }

        public void GeneraIndicador(object sender, EventArgs e)
        {
            indicadores.Visible = true;
            tablero.Visible = false;
            general.Visible = false;
            valor1.Text = "";
            valor2.Text = "";

            LinkButton l = (LinkButton)sender;
            List<Indicador> li = new List<Indicador>();

            String anio = anios.SelectedValue;
            String mes = meses.SelectedValue;
            String distrito = distritos.SelectedValue == "0" ? "" : distritos.SelectedValue;
            String juzgado = juzgados.SelectedValue == "0" ? "" : juzgados.SelectedValue;
            String nomuzgado = juzgados.SelectedValue == "0" ? "Totalidad de Tribunales" : juzgados.SelectedItem.Text;
            
            String tituloL, tituloLineas, yTitulo, tituloP;


            switch (l.ID)
            {
                case "ind1":

                    li = Indicadores.TraeInformacion(1, anio, mes, distrito, juzgado);

                    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    indicador.Text = "Proporción de quejas relativas al proceso de atención al público en " +
                        "relación con el número de demandas(ingresos) recibidas durante el mes";
                    valor1info.Text = "Número de quejas";
                    valor2info.Text = "Total de ingresos";
                    calculoinfo.Text = "Porcentaje de quejas";
                    valor1.Text = li[li.Count - 1].Val1.ToString();
                    valor2.Text = li[li.Count - 1].Val2.ToString();
                    calculo.Text = li[li.Count - 1].Calculo.ToString();

                    tituloL = "Quejas del servicio de atención al público";
                    tituloLineas = "Porcentaje de quejas";
                    yTitulo = "Proporción quejas";
                    arriba.Text = "Número de quejas";
                    abajo.Text = "Total de ingresos";
                    por.Text = "X 100";

                    String fn1 = "Lineas(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                        + tituloLineas + "', '" + yTitulo + "'); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "L", fn1, true);

                    //----------------------- pastel ----------------------------------------//
                    tituloP = tituloLineas;

                    List<String> titulos = new List<String>{
                        "Proporción de quejas",
                        "Proporción solicitudes que no son queja",
                    }; ;

                    List<float> calculos = new List<float>{
                        li[li.Count - 1].Calculo,
                        (100- li[li.Count - 1].Calculo),
                    }; ;

                    List<String> colores = new List<String>{
                        "#65baaf",
                        "#ea6852",
                    }; ;

                    String fn2 = "Pastel('" + tituloP + "', " + JsonConvert.SerializeObject(titulos) + ", " +
                        JsonConvert.SerializeObject(calculos) + ", " + JsonConvert.SerializeObject(colores) + "); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "P", fn2, true);

                    break;

                case "ind2":

                    li = Indicadores.TraeInformacion(2, anio, mes, distrito, juzgado);

                    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    indicador.Text = "Promedio de horas requeridas para emitir  " +
                        "un acuerdo derivado de la recepción de una actuación";
                    valor1info.Text = "Horas transcurridas";
                    valor2info.Text = "Total de acuerdos en el registro";
                    calculoinfo.Text = "Promedio en horas";
                    valor1.Text = li[li.Count - 1].Val1.ToString();
                    valor2.Text = li[li.Count - 1].Val2.ToString();
                    calculo.Text = li[li.Count - 1].Calculo.ToString();
                    arriba.Text = "E (Fecha de emisión del acuerdo - Fecha de recepción de la actuación (+1))";
                    abajo.Text = "Total de acuerdos en el registro";
                    por.Text = "";

                    tituloL = "Emitir un acuerdo derivado de la recepción de una actuación";
                    tituloLineas = "Promedio de horas";
                    yTitulo = "Proporcion en horas";

                    String fn3 = "Lineas(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                        + tituloLineas + "', '" + yTitulo + "'); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "L", fn3, true);

                    //----------------------- pastel ----------------------------------------//
                    tituloP = tituloLineas;

                    List<String> titulos1 = new List<String>{
                        "Promedio de horas",
                        "Proporcion de horas",
                    }; ;

                    List<float> calculos1 = new List<float>{
                        li[li.Count - 1].Calculo
                    }; ;

                    List<String> colores1 = new List<String>{
                        "#65baaf"
                    };

                    String fn4 = "Pastel('" + tituloP + "', " + JsonConvert.SerializeObject(titulos1) + ", " +
                        JsonConvert.SerializeObject(calculos1) + ", " + JsonConvert.SerializeObject(colores1) + "); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "P", fn4, true);
                    break;

                case "ind3":

                    li = Indicadores.TraeInformacion(3, anio, mes, distrito, juzgado);

                    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    indicador.Text = "Promedio de horas requeridas para notificar personalmente un acuerdo ";
                    valor1info.Text = "Horas transcurridas";
                    valor2info.Text = "Total de notificaciones";
                    calculoinfo.Text = "Promedio en horas";
                    valor1.Text = li[li.Count - 1].Val1.ToString();
                    valor2.Text = li[li.Count - 1].Val2.ToString();
                    calculo.Text = li[li.Count - 1].Calculo.ToString();

                    tituloL = "Notificar personalmente un acuerdo";
                    tituloLineas = "Promedio de horas";
                    yTitulo = "Proporcion en horas";
                    arriba.Text = "E (Fecha de la notificación - Fecha de emisión del acuerdo (+1))";
                    abajo.Text = "Total de notificaciones";
                    por.Text = "";

                    String fn5 = "Lineas(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                        + tituloLineas + "', '" + yTitulo + "'); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "L", fn5, true);

                    //----------------------- pastel ----------------------------------------//
                    tituloP = tituloLineas;

                    List<String> titulos2 = new List<String>{
                        "Promedio de horas",
                        "Proporcion de horas",
                    }; ;

                    List<float> calculos2 = new List<float>{
                        li[li.Count - 1].Calculo
                    }; ;

                    List<String> colores2 = new List<String>{
                        "#65baaf"
                    }; 

                    String fn6 = "Pastel('" + tituloP + "', " + JsonConvert.SerializeObject(titulos2) + ", " +
                        JsonConvert.SerializeObject(calculos2) + ", " + JsonConvert.SerializeObject(colores2) + "); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "P", fn6, true);
                    break;

                case "ind4":

                    li = Indicadores.TraeInformacion(4, anio, mes, distrito, juzgado);

                    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    indicador.Text = "Porcentaje de audiencias efectivamente celebradas en relación con  " +
                        "las audiencias señaladas buscando contrarestar el diferimiento innecesario";
                    valor1info.Text = "Total de audiencias celebradas";
                    valor2info.Text = "Total de audiencias programadas";
                    calculoinfo.Text = "Porcentaje de Audiencias";
                    valor1.Text = li[li.Count - 1].Val1.ToString();
                    valor2.Text = li[li.Count - 1].Val2.ToString();
                    calculo.Text = li[li.Count - 1].Calculo.ToString();

                    tituloL = "Porcentaje de audiencias efectivamente celebradas";
                    tituloLineas = "Porcentaje de Audiencias";
                    yTitulo = "Proporcion en Audiencias";
                    arriba.Text = "Total de audiencias celebradas";
                    abajo.Text = "Total de audiencias programadas";
                    por.Text = "X 100";

                    String fn7 = "Lineas(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                        + tituloLineas + "', '" + yTitulo + "'); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "L", fn7, true);

                    //----------------------- pastel ----------------------------------------//
                    tituloP = tituloLineas;

                    List<String> titulos3 = new List<String>{
                        "Promedio de Audiencias",
                        "Proporcion de Audiencias",
                    }; ;

                    List<float> calculos3 = new List<float>{
                        li[li.Count - 1].Calculo,
                        (100 - (li[li.Count - 1].Calculo)),
                    }; ;

                    List<String> colores3 = new List<String>{
                        "#65baaf",
                        "#ea6852"
                    }; ;


                    String fn8 = "Pastel('" + tituloP + "', " + JsonConvert.SerializeObject(titulos3) + ", " +
                        JsonConvert.SerializeObject(calculos3) + ", " + JsonConvert.SerializeObject(colores3) + "); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "P", fn8, true);
                    break;

                case "ind5":

                    li = Indicadores.TraeInformacion(5, anio, mes, distrito, juzgado);

                    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    indicador.Text = "Proporción de asuntos que tienen una resolución cumplida.  ";
                    valor1info.Text = "Total de expedientes";
                    valor2info.Text = "Total de resoluciones cumplidas";
                    calculoinfo.Text = "Porcentaje de Asuntos Resueltos";
                    valor1.Text = li[li.Count - 1].Val1.ToString();
                    valor2.Text = li[li.Count - 1].Val2.ToString();
                    calculo.Text = li[li.Count - 1].Calculo.ToString();

                    tituloL = "Asuntos que tienen una resolución cumplida";
                    tituloLineas = "Porcentaje de Asuntos";
                    yTitulo = "Proporcion de Asuntos";
                    arriba.Text = "Total de resoluciones cumplidas";
                    abajo.Text = "Total de expedientes";
                    por.Text = "X 100";

                    String fn9 = "Lineas(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                        + tituloLineas + "', '" + yTitulo + "'); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "L", fn9, true);

                    //----------------------- pastel ----------------------------------------//
                    tituloP = tituloLineas;

                    List<String> titulos4 = new List<String>{
                        "Porcentaje de Asuntos",
                        "Proporcion de Asuntos",
                    }; ;

                    List<float> calculos4 = new List<float>{
                        li[li.Count - 1].Calculo,
                        (100- li[li.Count - 1].Calculo),
                    }; ;

                    List<String> colores4 = new List<String>{
                        "#65baaf",
                        "#ea6852"
                    };

                    String fn10 = "Pastel('" + tituloP + "', " + JsonConvert.SerializeObject(titulos4) + ", " +
                        JsonConvert.SerializeObject(calculos4) + ", " + JsonConvert.SerializeObject(colores4) + "); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "P", fn10, true);

                    break;

                //NUEVOS INDICADORES -------------------------------------------------------------------------------------------------//

                case "indotro1":

                    li = Indicadores.TraeInformacion(1, anio, mes, distrito, juzgado);

                    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    indicador.Text = "Proporción de quejas relativas al proceso de atención al público en " +
                        "relación con el número de demandas y actuaciones (ingresos) recibidas durante el mes";
                    valor1info.Text = "Número de quejas";
                    valor2info.Text = "Total de ingresos";
                    calculoinfo.Text = "Porcentaje de quejas";
                    valor1.Text = li[li.Count - 1].Val1.ToString();
                    valor2.Text = li[li.Count - 1].Val2.ToString();
                    calculo.Text = li[li.Count - 1].Calculo.ToString();

                    tituloL = "Quejas del servicio de atención al público";
                    tituloLineas = "Porcentaje de quejas";
                    yTitulo = "Proporción quejas";
                    arriba.Text = "Número de quejas";
                    abajo.Text = "Total de ingresos";
                    por.Text = "X 100";

                    String fn11 = "Lineas(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                        + tituloLineas + "', '" + yTitulo + "'); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "L", fn11, true);

                    //----------------------- pastel ----------------------------------------//
                    tituloP = tituloLineas;

                    List<String> titulos5 = new List<String>{
                        "Proporción de quejas",
                        "Proporción solicitudes que no son queja",
                    }; ;

                    List<float> calculos5 = new List<float>{
                        li[li.Count - 1].Calculo,
                        (100- li[li.Count - 1].Calculo),
                    }; ;

                    List<String> colores5 = new List<String>{
                        "#65baaf",
                        "#ea6852",
                    }; ;

                    String fn12 = "Pastel('" + tituloP + "', " + JsonConvert.SerializeObject(titulos5) + ", " +
                        JsonConvert.SerializeObject(calculos5) + ", " + JsonConvert.SerializeObject(colores5) + "); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "P", fn12, true);

                    break;

                case "indotro2":

                    li = Indicadores.TraeInformacion(2, anio, mes, distrito, juzgado);

                    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    indicador.Text = "Promedio de horas requeridas para emitir  " +
                        "un acuerdo derivado de la recepción de una actuación";
                    valor1info.Text = "Fecha de recepción de la actuación (+1)";
                    valor2info.Text = "Fecha de emisión del acuerdo";
                    calculoinfo.Text = "Promedio en horas";
                    valor1.Text = li[li.Count - 1].Val1.ToString();
                    valor2.Text = li[li.Count - 1].Val2.ToString();
                    calculo.Text = li[li.Count - 1].Calculo.ToString();
                    arriba.Text = "E (Fecha de emisión del acuerdo - Fecha de recepción de la actuación (+1))";
                    abajo.Text = "Total de acuerdos en el registro";
                    por.Text = "";

                    tituloL = "Emitir un acuerdo derivado de la recepción de una actuación";
                    tituloLineas = "Promedio de horas";
                    yTitulo = "Proporcion en horas";

                    String fn13 = "Lineas(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                        + tituloLineas + "', '" + yTitulo + "'); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "L", fn13, true);

                    //----------------------- pastel ----------------------------------------//
                    tituloP = tituloLineas;

                    List<String> titulos6 = new List<String>{
                        "Promedio de horas",
                        "Proporcion de horas",
                    }; ;

                    List<float> calculos6 = new List<float>{
                        li[li.Count - 1].Calculo
                    }; ;

                    List<String> colores6 = new List<String>{
                        "#65baaf"
                    };

                    String fn14 = "Pastel('" + tituloP + "', " + JsonConvert.SerializeObject(titulos6) + ", " +
                        JsonConvert.SerializeObject(calculos6) + ", " + JsonConvert.SerializeObject(colores6) + "); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "P", fn14, true);
                    break;

                case "indotro3":

                    li = Indicadores.TraeInformacion(3, anio, mes, distrito, juzgado);

                    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    indicador.Text = "Promedio de horas requeridas para notificar personalmente un acuerdo ";
                    valor1info.Text = "Fecha de emisión del acuerdo (+1)";
                    valor2info.Text = "Fecha de la notificación";
                    calculoinfo.Text = "Promedio en horas";
                    valor1.Text = li[li.Count - 1].Val1.ToString();
                    valor2.Text = li[li.Count - 1].Val2.ToString();
                    calculo.Text = li[li.Count - 1].Calculo.ToString();

                    tituloL = "Notificar personalmente un acuerdo";
                    tituloLineas = "Promedio de horas";
                    yTitulo = "Proporcion en horas";
                    arriba.Text = "E (Fecha de la notificación - Fecha de emisión del acuerdo (+1))";
                    abajo.Text = "Total de notificaciones";
                    por.Text = "";

                    String fn15 = "Lineas(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                        + tituloLineas + "', '" + yTitulo + "'); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "L", fn15, true);

                    //----------------------- pastel ----------------------------------------//
                    tituloP = tituloLineas;

                    List<String> titulos7 = new List<String>{
                        "Promedio de horas",
                        "Proporcion de horas",
                    }; ;

                    List<float> calculos7 = new List<float>{
                        li[li.Count - 1].Calculo
                    }; ;

                    List<String> colores7 = new List<String>{
                        "#65baaf"
                    };

                    String fn16 = "Pastel('" + tituloP + "', " + JsonConvert.SerializeObject(titulos7) + ", " +
                        JsonConvert.SerializeObject(calculos7) + ", " + JsonConvert.SerializeObject(colores7) + "); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "P", fn16, true);
                    break;

                case "indotro4":

                    li = Indicadores.TraeInformacion(4, anio, mes, distrito, juzgado);

                    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    indicador.Text = "Promedio de audiencias efectivamente celebradas en relación con  " +
                        "las audiencias señaladas buscando contrarestar el diferimiento innecesario";
                    valor1info.Text = "Total de audiencias celebradas";
                    valor2info.Text = "Total de audiencias programadas";
                    calculoinfo.Text = "Promedio de Audiencias";
                    valor1.Text = li[li.Count - 1].Val1.ToString();
                    valor2.Text = li[li.Count - 1].Val2.ToString();
                    calculo.Text = li[li.Count - 1].Calculo.ToString();

                    tituloL = "Promedio de audiencias efectivamente celebradas";
                    tituloLineas = "Promedio de Audiencias";
                    yTitulo = "Proporcion en Audiencias";
                    arriba.Text = "Total de audiencias celebradas";
                    abajo.Text = "Total de audiencias programadas";
                    por.Text = "X 100";

                    String fn17 = "Lineas(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                        + tituloLineas + "', '" + yTitulo + "'); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "L", fn17, true);

                    //----------------------- pastel ----------------------------------------//
                    tituloP = tituloLineas;

                    List<String> titulos8 = new List<String>{
                        "Promedio de Audiencias",
                        "Proporcion de Audiencias",
                    }; ;

                    List<float> calculos8 = new List<float>{
                        li[li.Count - 1].Calculo,
                        (100- li[li.Count - 1].Calculo),
                    }; ;

                    List<String> colores8 = new List<String>{
                        "#65baaf",
                        "#ea6852"
                    }; ;


                    String fn18 = "Pastel('" + tituloP + "', " + JsonConvert.SerializeObject(titulos8) + ", " +
                        JsonConvert.SerializeObject(calculos8) + ", " + JsonConvert.SerializeObject(colores8) + "); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "P", fn18, true);
                    break;

                case "indotro5":

                    li = Indicadores.TraeInformacion(5, anio, mes, distrito, juzgado);

                    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    indicador.Text = "Proporción de asuntos que tienen una resolución cumplida.  ";
                    valor1info.Text = "Total de expedientes";
                    valor2info.Text = "Total de resoluciones cumplidas";
                    calculoinfo.Text = "Porcentaje de Asuntos";
                    valor1.Text = li[li.Count - 1].Val1.ToString();
                    valor2.Text = li[li.Count - 1].Val2.ToString();
                    calculo.Text = li[li.Count - 1].Calculo.ToString();

                    tituloL = "Asuntos que tienen una resolución cumplida";
                    tituloLineas = "Porcentaje de Asuntos";
                    yTitulo = "Proporcion de Asuntos";
                    arriba.Text = "Total de resoluciones cumplidas";
                    abajo.Text = "Total de expedientes";
                    por.Text = "X 100";

                    String fn19 = "Lineas(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                        + tituloLineas + "', '" + yTitulo + "'); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "L", fn19, true);

                    //----------------------- pastel ----------------------------------------//
                    tituloP = tituloLineas;

                    List<String> titulos9 = new List<String>{
                        "Porcentaje de Asuntos",
                        "Proporcion de Asuntos",
                    }; ;

                    List<float> calculos9 = new List<float>{
                        li[li.Count - 1].Calculo,
                        (100- li[li.Count - 1].Calculo),
                    }; ;

                    List<String> colores9 = new List<String>{
                        "#65baaf",
                        "#ea6852"
                    };

                    String fn20 = "Pastel('" + tituloP + "', " + JsonConvert.SerializeObject(titulos9) + ", " +
                        JsonConvert.SerializeObject(calculos9) + ", " + JsonConvert.SerializeObject(colores9) + "); ";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "P", fn20, true);

                    break;
            }
        }

        public void ParametrosTablero(object sender, EventArgs e)
        {
            tablero.Visible = true;
            indicadores.Visible = false;
            general.Visible = false;

            eneInd1.Text = "";
            febInd1.Text = "";
            marInd1.Text = "";
            abrInd1.Text = "";
            mayInd1.Text = "";
            junInd1.Text = "";
            julInd1.Text = "";
            agoInd1.Text = "";
            sepInd1.Text = "";
            octInd1.Text = "";
            novInd1.Text = "";
            dicInd1.Text = "";

            eneInd2.Text = "";
            febInd2.Text = "";
            marInd2.Text = "";
            abrInd2.Text = "";
            mayInd2.Text = "";
            junInd2.Text = "";
            julInd2.Text = "";
            agoInd2.Text = "";
            sepInd2.Text = "";
            octInd2.Text = "";
            novInd2.Text = "";
            dicInd2.Text = "";

            eneInd3.Text = "";
            febInd3.Text = "";
            marInd3.Text = "";
            abrInd3.Text = "";
            mayInd3.Text = "";
            junInd3.Text = "";
            julInd3.Text = "";
            agoInd3.Text = "";
            sepInd3.Text = "";
            octInd3.Text = "";
            novInd3.Text = "";
            dicInd3.Text = "";

            eneInd4.Text = "";
            febInd4.Text = "";
            marInd4.Text = "";
            abrInd4.Text = "";
            mayInd4.Text = "";
            junInd4.Text = "";
            julInd4.Text = "";
            agoInd4.Text = "";
            sepInd4.Text = "";
            octInd4.Text = "";
            novInd4.Text = "";
            dicInd4.Text = "";

            eneInd5.Text = "";
            febInd5.Text = "";
            marInd5.Text = "";
            abrInd5.Text = "";
            mayInd5.Text = "";
            junInd5.Text = "";
            julInd5.Text = "";
            agoInd5.Text = "";
            sepInd5.Text = "";
            octInd5.Text = "";
            novInd5.Text = "";
            dicInd5.Text = "";

            string distrito = distritos.SelectedValue;
            string juzgado = juzgados.SelectedValue;
            string anio = anios.SelectedValue;

            string juzgadoNombre = juzgados.SelectedItem.ToString();
            if (juzgados.SelectedItem.ToString().Equals("Todos los Tribunales"))
            {
                juzgadoNombre = "Totalidad de Tribunales";
            }
            distritoNombre.Text = "Distrito: " + distritos.SelectedItem.ToString();
            juzgadoNom.Text = juzgadoNombre;
            anioTablero.Text = anio;

            Tablero t = new Tablero();
            if (anio.Equals("2020"))
            {
                List<TableroM> list = t.LlenarTabla2(distrito, anio, juzgado);
                foreach (TableroM tab in list)
                {

                    if (tab.Indicador.Equals("1"))
                    {
                        switch (tab.Mes)
                        {
                            case "1":
                                eneInd1.Text = tab.Calculo.ToString();
                                eneInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "2":
                                febInd1.Text = tab.Calculo.ToString();
                                febInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "3":
                                marInd1.Text = tab.Calculo.ToString();
                                marInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "4":
                                abrInd1.Text = tab.Calculo.ToString();
                                abrInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "5":
                                mayInd1.Text = tab.Calculo.ToString();
                                mayInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "6":
                                junInd1.Text = tab.Calculo.ToString();
                                junInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "7":
                                julInd1.Text = tab.Calculo.ToString();
                                julInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "8":
                                agoInd1.Text = tab.Calculo.ToString();
                                agoInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "9":
                                sepInd1.Text = tab.Calculo.ToString();
                                sepInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "10":
                                octInd1.Text = tab.Calculo.ToString();
                                octInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "11":
                                novInd1.Text = tab.Calculo.ToString();
                                novInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "12":
                                dicInd1.Text = tab.Calculo.ToString();
                                dicInd1.Attributes.Add("class", tab.Estado);
                                break;
                        }
                    }
                    if (tab.Indicador.Equals("2"))
                    {
                        switch (tab.Mes)
                        {
                            case "1":
                                eneInd2.Text = tab.Calculo.ToString();
                                eneInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "2":
                                febInd2.Text = tab.Calculo.ToString();
                                febInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "3":
                                marInd2.Text = tab.Calculo.ToString();
                                marInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "4":
                                abrInd2.Text = tab.Calculo.ToString();
                                abrInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "5":
                                mayInd2.Text = tab.Calculo.ToString();
                                mayInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "6":
                                junInd2.Text = tab.Calculo.ToString();
                                junInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "7":
                                julInd2.Text = tab.Calculo.ToString();
                                julInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "8":
                                agoInd2.Text = tab.Calculo.ToString();
                                agoInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "9":
                                sepInd2.Text = tab.Calculo.ToString();
                                sepInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "10":
                                octInd2.Text = tab.Calculo.ToString();
                                octInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "11":
                                novInd2.Text = tab.Calculo.ToString();
                                novInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "12":
                                dicInd2.Text = tab.Calculo.ToString();
                                dicInd2.Attributes.Add("class", tab.Estado);
                                break;
                        }
                    }
                    if (tab.Indicador.Equals("3"))
                    {
                        switch (tab.Mes)
                        {
                            case "1":
                                eneInd3.Text = tab.Calculo.ToString();
                                eneInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "2":
                                febInd3.Text = tab.Calculo.ToString();
                                febInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "3":
                                marInd3.Text = tab.Calculo.ToString();
                                marInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "4":
                                abrInd3.Text = tab.Calculo.ToString();
                                abrInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "5":
                                mayInd3.Text = tab.Calculo.ToString();
                                mayInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "6":
                                junInd3.Text = tab.Calculo.ToString();
                                junInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "7":
                                julInd3.Text = tab.Calculo.ToString();
                                julInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "8":
                                agoInd3.Text = tab.Calculo.ToString();
                                agoInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "9":
                                sepInd3.Text = tab.Calculo.ToString();
                                sepInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "10":
                                octInd3.Text = tab.Calculo.ToString();
                                octInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "11":
                                novInd3.Text = tab.Calculo.ToString();
                                novInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "12":
                                dicInd3.Text = tab.Calculo.ToString();
                                dicInd3.Attributes.Add("class", tab.Estado);
                                break;
                        }
                    }
                    if (tab.Indicador.Equals("4"))
                    {
                        switch (tab.Mes)
                        {
                            case "1":
                                eneInd4.Text = tab.Calculo.ToString();
                                eneInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "2":
                                febInd4.Text = tab.Calculo.ToString();
                                febInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "3":
                                marInd4.Text = tab.Calculo.ToString();
                                marInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "4":
                                abrInd4.Text = tab.Calculo.ToString();
                                abrInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "5":
                                abrInd4.Text = tab.Calculo.ToString();
                                abrInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "6":
                                junInd4.Text = tab.Calculo.ToString();
                                junInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "7":
                                julInd4.Text = tab.Calculo.ToString();
                                julInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "8":
                                agoInd4.Text = tab.Calculo.ToString();
                                agoInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "9":
                                sepInd4.Text = tab.Calculo.ToString();
                                sepInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "10":
                                octInd4.Text = tab.Calculo.ToString();
                                octInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "11":
                                novInd4.Text = tab.Calculo.ToString();
                                novInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "12":
                                dicInd4.Text = tab.Calculo.ToString();
                                dicInd4.Attributes.Add("class", tab.Estado);
                                break;
                        }
                    }
                    if (tab.Indicador.Equals("5"))
                    {
                        switch (tab.Mes)
                        {
                            case "1":
                                eneInd5.Text = tab.Calculo.ToString();
                                eneInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "2":
                                febInd5.Text = tab.Calculo.ToString();
                                febInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "3":
                                marInd5.Text = tab.Calculo.ToString();
                                marInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "4":
                                abrInd5.Text = tab.Calculo.ToString();
                                abrInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "5":
                                mayInd5.Text = tab.Calculo.ToString();
                                mayInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "6":
                                junInd5.Text = tab.Calculo.ToString();
                                junInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "7":
                                julInd5.Text = tab.Calculo.ToString();
                                julInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "8":
                                agoInd5.Text = tab.Calculo.ToString();
                                agoInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "9":
                                sepInd5.Text = tab.Calculo.ToString();
                                sepInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "10":
                                octInd5.Text = tab.Calculo.ToString();
                                octInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "11":
                                novInd5.Text = tab.Calculo.ToString();
                                novInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "12":
                                dicInd5.Text = tab.Calculo.ToString();
                                dicInd5.Attributes.Add("class", tab.Estado);
                                break;
                        }
                    }
 
                }
            }
            else
            {
                List<TableroM> list = t.LlenarTabla(distrito, anio, juzgado);
                foreach (TableroM tab in list)
                {

                    if (tab.Indicador.Equals("1"))
                    {
                        switch (tab.Mes)
                        {
                            case "1":
                                eneInd1.Text = tab.Calculo.ToString();
                                eneInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "2":
                                febInd1.Text = tab.Calculo.ToString();
                                febInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "3":
                                marInd1.Text = tab.Calculo.ToString();
                                marInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "4":
                                abrInd1.Text = tab.Calculo.ToString();
                                abrInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "5":
                                mayInd1.Text = tab.Calculo.ToString();
                                mayInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "6":
                                junInd1.Text = tab.Calculo.ToString();
                                junInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "7":
                                julInd1.Text = tab.Calculo.ToString();
                                julInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "8":
                                agoInd1.Text = tab.Calculo.ToString();
                                agoInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "9":
                                sepInd1.Text = tab.Calculo.ToString();
                                sepInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "10":
                                octInd1.Text = tab.Calculo.ToString();
                                octInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "11":
                                novInd1.Text = tab.Calculo.ToString();
                                novInd1.Attributes.Add("class", tab.Estado);
                                break;
                            case "12":
                                dicInd1.Text = tab.Calculo.ToString();
                                dicInd1.Attributes.Add("class", tab.Estado);
                                break;
                        }
                    }
                    if (tab.Indicador.Equals("2"))
                    {
                        switch (tab.Mes)
                        {
                            case "1":
                                eneInd2.Text = tab.Calculo.ToString();
                                eneInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "2":
                                febInd2.Text = tab.Calculo.ToString();
                                febInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "3":
                                marInd2.Text = tab.Calculo.ToString();
                                marInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "4":
                                abrInd2.Text = tab.Calculo.ToString();
                                abrInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "5":
                                mayInd2.Text = tab.Calculo.ToString();
                                mayInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "6":
                                junInd2.Text = tab.Calculo.ToString();
                                junInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "7":
                                julInd2.Text = tab.Calculo.ToString();
                                julInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "8":
                                agoInd2.Text = tab.Calculo.ToString();
                                agoInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "9":
                                sepInd2.Text = tab.Calculo.ToString();
                                sepInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "10":
                                octInd2.Text = tab.Calculo.ToString();
                                octInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "11":
                                novInd2.Text = tab.Calculo.ToString();
                                novInd2.Attributes.Add("class", tab.Estado);
                                break;
                            case "12":
                                dicInd2.Text = tab.Calculo.ToString();
                                dicInd2.Attributes.Add("class", tab.Estado);
                                break;
                        }
                    }
                    if (tab.Indicador.Equals("3"))
                    {
                        switch (tab.Mes)
                        {
                            case "1":
                                eneInd3.Text = tab.Calculo.ToString();
                                eneInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "2":
                                febInd3.Text = tab.Calculo.ToString();
                                febInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "3":
                                marInd3.Text = tab.Calculo.ToString();
                                marInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "4":
                                abrInd3.Text = tab.Calculo.ToString();
                                abrInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "5":
                                mayInd3.Text = tab.Calculo.ToString();
                                mayInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "6":
                                junInd3.Text = tab.Calculo.ToString();
                                junInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "7":
                                julInd3.Text = tab.Calculo.ToString();
                                julInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "8":
                                agoInd3.Text = tab.Calculo.ToString();
                                agoInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "9":
                                sepInd3.Text = tab.Calculo.ToString();
                                sepInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "10":
                                octInd3.Text = tab.Calculo.ToString();
                                octInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "11":
                                novInd3.Text = tab.Calculo.ToString();
                                novInd3.Attributes.Add("class", tab.Estado);
                                break;
                            case "12":
                                dicInd3.Text = tab.Calculo.ToString();
                                dicInd3.Attributes.Add("class", tab.Estado);
                                break;
                        }
                    }
                    if (tab.Indicador.Equals("4"))
                    {
                        switch (tab.Mes)
                        {
                            case "1":
                                eneInd4.Text = tab.Calculo.ToString();
                                eneInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "2":
                                febInd4.Text = tab.Calculo.ToString();
                                febInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "3":
                                marInd4.Text = tab.Calculo.ToString();
                                marInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "4":
                                abrInd4.Text = tab.Calculo.ToString();
                                abrInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "5":
                                mayInd4.Text = tab.Calculo.ToString();
                                mayInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "6":
                                junInd4.Text = tab.Calculo.ToString();
                                junInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "7":
                                julInd4.Text = tab.Calculo.ToString();
                                julInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "8":
                                agoInd4.Text = tab.Calculo.ToString();
                                agoInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "9":
                                sepInd4.Text = tab.Calculo.ToString();
                                sepInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "10":
                                octInd4.Text = tab.Calculo.ToString();
                                octInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "11":
                                novInd4.Text = tab.Calculo.ToString();
                                novInd4.Attributes.Add("class", tab.Estado);
                                break;
                            case "12":
                                dicInd4.Text = tab.Calculo.ToString();
                                dicInd4.Attributes.Add("class", tab.Estado);
                                break;
                        }
                    }
                    if (tab.Indicador.Equals("5"))
                    {
                        switch (tab.Mes)
                        {
                            case "1":
                                eneInd5.Text = tab.Calculo.ToString();
                                eneInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "2":
                                febInd5.Text = tab.Calculo.ToString();
                                febInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "3":
                                marInd5.Text = tab.Calculo.ToString();
                                marInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "4":
                                abrInd5.Text = tab.Calculo.ToString();
                                abrInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "5":
                                mayInd5.Text = tab.Calculo.ToString();
                                mayInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "6":
                                junInd5.Text = tab.Calculo.ToString();
                                junInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "7":
                                julInd5.Text = tab.Calculo.ToString();
                                julInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "8":
                                agoInd5.Text = tab.Calculo.ToString();
                                agoInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "9":
                                sepInd5.Text = tab.Calculo.ToString();
                                sepInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "10":
                                octInd5.Text = tab.Calculo.ToString();
                                octInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "11":
                                novInd5.Text = tab.Calculo.ToString();
                                novInd5.Attributes.Add("class", tab.Estado);
                                break;
                            case "12":
                                dicInd5.Text = tab.Calculo.ToString();
                                dicInd5.Attributes.Add("class", tab.Estado);
                                break;
                        }
                    }



                    //}
                    //------------------NUEVOS INDICADORES -----------------------------------//
                    //if (tab.Indicador.Equals("6"))
                    //{
                    //    switch (tab.Mes)
                    //    {
                    //        case "1":
                    //            eneInd6.Text = tab.Calculo.ToString();
                    //            eneInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "2":
                    //            febInd6.Text = tab.Calculo.ToString();
                    //            febInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "3":
                    //            marInd6.Text = tab.Calculo.ToString();
                    //            marInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "4":
                    //            abrInd6.Text = tab.Calculo.ToString();
                    //            abrInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "5":
                    //            mayInd6.Text = tab.Calculo.ToString();
                    //            mayInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "6":
                    //            junInd6.Text = tab.Calculo.ToString();
                    //            junInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "7":
                    //            julInd6.Text = tab.Calculo.ToString();
                    //            julInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "8":
                    //            agoInd5.Text = tab.Calculo.ToString();
                    //            agoInd5.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "9":
                    //            sepInd6.Text = tab.Calculo.ToString();
                    //            sepInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "10":
                    //            octInd6.Text = tab.Calculo.ToString();
                    //            octInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "11":
                    //            novInd6.Text = tab.Calculo.ToString();
                    //            novInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "12":
                    //            dicInd6.Text = tab.Calculo.ToString();
                    //            dicInd6.Attributes.Add("class", tab.Estado);
                    //            break;
                    //    }
                    //}
                    //if (tab.Indicador.Equals("7"))
                    //{
                    //    switch (tab.Mes)
                    //    {
                    //        case "1":
                    //            eneInd7.Text = tab.Calculo.ToString();
                    //            eneInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "2":
                    //            febInd7.Text = tab.Calculo.ToString();
                    //            febInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "3":
                    //            marInd7.Text = tab.Calculo.ToString();
                    //            marInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "4":
                    //            abrInd7.Text = tab.Calculo.ToString();
                    //            abrInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "5":
                    //            mayInd7.Text = tab.Calculo.ToString();
                    //            mayInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "6":
                    //            junInd7.Text = tab.Calculo.ToString();
                    //            junInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "7":
                    //            julInd7.Text = tab.Calculo.ToString();
                    //            julInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "8":
                    //            agoInd7.Text = tab.Calculo.ToString();
                    //            agoInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "9":
                    //            sepInd7.Text = tab.Calculo.ToString();
                    //            sepInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "10":
                    //            octInd7.Text = tab.Calculo.ToString();
                    //            octInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "11":
                    //            novInd7.Text = tab.Calculo.ToString();
                    //            novInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "12":
                    //            dicInd7.Text = tab.Calculo.ToString();
                    //            dicInd7.Attributes.Add("class", tab.Estado);
                    //            break;
                    //    }
                    //}
                    //if (tab.Indicador.Equals("8"))
                    //{
                    //    switch (tab.Mes)
                    //    {
                    //        case "1":
                    //            eneInd8.Text = tab.Calculo.ToString();
                    //            eneInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "2":
                    //            febInd8.Text = tab.Calculo.ToString();
                    //            febInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "3":
                    //            marInd8.Text = tab.Calculo.ToString();
                    //            marInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "4":
                    //            abrInd8.Text = tab.Calculo.ToString();
                    //            abrInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "5":
                    //            mayInd8.Text = tab.Calculo.ToString();
                    //            mayInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "6":
                    //            junInd8.Text = tab.Calculo.ToString();
                    //            junInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "7":
                    //            julInd8.Text = tab.Calculo.ToString();
                    //            julInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "8":
                    //            agoInd8.Text = tab.Calculo.ToString();
                    //            agoInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "9":
                    //            sepInd8.Text = tab.Calculo.ToString();
                    //            sepInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "10":
                    //            octInd8.Text = tab.Calculo.ToString();
                    //            octInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "11":
                    //            novInd8.Text = tab.Calculo.ToString();
                    //            novInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "12":
                    //            dicInd8.Text = tab.Calculo.ToString();
                    //            dicInd8.Attributes.Add("class", tab.Estado);
                    //            break;
                    //    }
                    //}
                    //if (tab.Indicador.Equals("9"))
                    //{
                    //    switch (tab.Mes)
                    //    {
                    //        case "1":
                    //            eneInd9.Text = tab.Calculo.ToString();
                    //            eneInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "2":
                    //            febInd9.Text = tab.Calculo.ToString();
                    //            febInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "3":
                    //            marInd9.Text = tab.Calculo.ToString();
                    //            marInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "4":
                    //            abrInd9.Text = tab.Calculo.ToString();
                    //            abrInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "5":
                    //            mayInd9.Text = tab.Calculo.ToString();
                    //            mayInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "6":
                    //            junInd9.Text = tab.Calculo.ToString();
                    //            junInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "7":
                    //            julInd9.Text = tab.Calculo.ToString();
                    //            julInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "8":
                    //            agoInd9.Text = tab.Calculo.ToString();
                    //            agoInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "9":
                    //            sepInd9.Text = tab.Calculo.ToString();
                    //            sepInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "10":
                    //            octInd9.Text = tab.Calculo.ToString();
                    //            octInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "11":
                    //            novInd9.Text = tab.Calculo.ToString();
                    //            novInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "12":
                    //            dicInd9.Text = tab.Calculo.ToString();
                    //            dicInd9.Attributes.Add("class", tab.Estado);
                    //            break;
                    //    }
                    //}
                    //if (tab.Indicador.Equals("10"))
                    //{
                    //    switch (tab.Mes)
                    //    {
                    //        case "1":
                    //            eneInd10.Text = tab.Calculo.ToString();
                    //            eneInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "2":
                    //            febInd10.Text = tab.Calculo.ToString();
                    //            febInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "3":
                    //            marInd10.Text = tab.Calculo.ToString();
                    //            marInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "4":
                    //            abrInd10.Text = tab.Calculo.ToString();
                    //            abrInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "5":
                    //            mayInd10.Text = tab.Calculo.ToString();
                    //            mayInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "6":
                    //            junInd10.Text = tab.Calculo.ToString();
                    //            junInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "7":
                    //            julInd10.Text = tab.Calculo.ToString();
                    //            julInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "8":
                    //            agoInd10.Text = tab.Calculo.ToString();
                    //            agoInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "9":
                    //            sepInd10.Text = tab.Calculo.ToString();
                    //            sepInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "10":
                    //            octInd10.Text = tab.Calculo.ToString();
                    //            octInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "11":
                    //            novInd10.Text = tab.Calculo.ToString();
                    //            novInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //        case "12":
                    //            dicInd10.Text = tab.Calculo.ToString();
                    //            dicInd10.Attributes.Add("class", tab.Estado);
                    //            break;
                    //    }
                    //}
                }
            }

            


        }

        public void GeneraIndicadores(object sender, EventArgs e)
        {
            indicadores.Visible = false;
            tablero.Visible = false;
            general.Visible = true;

            List<Indicador> li = new List<Indicador>();
            String anio = anios.SelectedValue;
            String mes = meses.SelectedValue;
            String distrito = distritos.SelectedValue == "0" ? "" : distritos.SelectedValue;
            String juzgado = juzgados.SelectedValue == "0" ? "" : juzgados.SelectedValue;
            String nomuzgado = juzgados.SelectedValue == "0" ? "Todos los Tribunales" : juzgados.SelectedItem.Text;

            distrito_ge.Text = "Distrito de " + distritos.SelectedItem.ToString();
            nom_ge.Text = nomuzgado;
            anio_ge.Text = anio;

            String tituloL, tituloLineas, yTitulo;

            for (int i = 1; i <= 10; i++)
            {
                switch (i)
                {
                    case 1:

                        li = Indicadores.TraeInformacion(1, anio, mes, distrito, juzgado);

                        distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                        indicador.Text = "Proporción de quejas relativas al proceso de atención al público en " +
                            "relación con el número de demandas y actuaciones (ingresos) recibidas durante el mes";

                        valor1ind1.Text = "Número de quejas: " + li[li.Count - 1].Val1.ToString();
                        valor2ind1.Text = "Total de ingresos: " + li[li.Count - 1].Val2.ToString();
                        calculoind1.Text = "Porcentaje de quejas: " + li[li.Count - 1].Calculo.ToString();

                        tituloL = "Quejas del servicio de atención al público";
                        tituloLineas = "Porcentaje de quejas";
                        yTitulo = "Proporción quejas";

                        String fn1 = "LineasGeneral(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                            + tituloLineas + "', '" + yTitulo + "','lineas_ind1'); ";
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "LG1", fn1, true);

                        break;

                    case 2:

                        li = Indicadores.TraeInformacion(2, anio, mes, distrito, juzgado);

                        distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                        indicador.Text = "Promedio de horas requeridas para emitir  " +
                            "un acuerdo derivado de la recepción de una actuación";
                        valor1ind2.Text = "Fecha de recepción de la actuación (+1): " + li[li.Count - 1].Val1.ToString();
                        valor2ind2.Text = "Fecha de emisión del acuerdo: " + li[li.Count - 1].Val2.ToString();
                        calculoind2.Text = "Promedio en horas : " + li[li.Count - 1].Calculo.ToString();

                        tituloL = "Emitir un acuerdo derivado de la recepción de una actuación";
                        tituloLineas = "Promedio de horas";
                        yTitulo = "Proporcion en horas";

                        String fn2 = "LineasGeneral(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                            + tituloLineas + "', '" + yTitulo + "','lineas_ind2'); ";
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "LG2", fn2, true);

                        break;

                    case 3:

                        li = Indicadores.TraeInformacion(3, anio, mes, distrito, juzgado);

                        distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                        indicador.Text = "Promedio de horas requeridas para notificar personalmente un acuerdo ";
                        valor1ind3.Text = "Fecha de emisión del acuerdo (+1): " + li[li.Count - 1].Val1.ToString();
                        valor2ind3.Text = "Fecha de la notificación: " + li[li.Count - 1].Val2.ToString();
                        calculoind3.Text = "Promedio en horas: " + li[li.Count - 1].Calculo.ToString();

                        tituloL = "Notificar personalmente un acuerdo";
                        tituloLineas = "Promedio de horas";
                        yTitulo = "Proporcion en horas";

                        String fn3 = "LineasGeneral(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                            + tituloLineas + "', '" + yTitulo + "','lineas_ind3'); ";
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "LG3", fn3, true);

                        break;

                    case 4:

                        li = Indicadores.TraeInformacion(4, anio, mes, distrito, juzgado);

                        distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                        indicador.Text = "Promedio de audiencias efectivamente celebradas en relación con  " +
                            "las audiencias señaladas buscando contrarestar el diferimiento innecesario";
                        valor1ind4.Text = "Total de audiencias celebradas: " + li[li.Count - 1].Val1.ToString();
                        valor2ind4.Text = "Total de audiencias programadas: " + li[li.Count - 1].Val2.ToString();
                        calculoind4.Text = "Promedio de Audiencias: " + li[li.Count - 1].Calculo.ToString();

                        tituloL = "Promedio de audiencias efectivamente celebradas";
                        tituloLineas = "Promedio de Audiencias";
                        yTitulo = "Proporcion en Audiencias";

                        String fn4 = "LineasGeneral(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                            + tituloLineas + "', '" + yTitulo + "','lineas_ind4'); ";
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "LG4", fn4, true);

                        break;

                    case 5:

                        li = Indicadores.TraeInformacion(5, anio, mes, distrito, juzgado);

                        distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                        indicador.Text = "Proporción de asuntos que tienen una resolución cumplida.  ";
                        valor1ind5.Text = "Total de expedientes: " + li[li.Count - 1].Val1.ToString();
                        valor2ind5.Text = "Total de resoluciones cumplidas: " + li[li.Count - 1].Val2.ToString();
                        calculoind5.Text = "Porcentaje de Asuntos: " + li[li.Count - 1].Calculo.ToString();

                        tituloL = "Asuntos que tienen una resolución cumplida";
                        tituloLineas = "Porcentaje de Asuntos";
                        yTitulo = "Proporcion de Asuntos";

                        String fn5 = "LineasGeneral(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                            + tituloLineas + "', '" + yTitulo + "','lineas_ind5'); ";
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "LG5", fn5, true);

                        break;

                    // Nuevos indicadores +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

                    //case 6:

                    //    li = Indicadores.TraeInformacion(1, anio, mes, distrito, juzgado);

                    //    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    //    indicador.Text = "Proporción de quejas relativas al proceso de atención al público en " +
                    //        "relación con el número de demandas y actuaciones (ingresos) recibidas durante el mes";

                    //    valor1ind6.Text = "Número de quejas: " + li[li.Count - 1].Val1.ToString();
                    //    valor2ind6.Text = "Total de ingresos: " + li[li.Count - 1].Val2.ToString();
                    //    calculoind6.Text = "Porcentaje de quejas: " + li[li.Count - 1].Calculo.ToString();

                    //    tituloL = "Quejas del servicio de atención al público";
                    //    tituloLineas = "Porcentaje de quejas";
                    //    yTitulo = "Proporción quejas";

                    //    String fn6 = "LineasGeneral(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                    //        + tituloLineas + "', '" + yTitulo + "','lineas_ind6'); ";
                    //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "LG6", fn6, true);

                    //    break;

                    //case 7:

                    //    li = Indicadores.TraeInformacion(2, anio, mes, distrito, juzgado);

                    //    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    //    indicador.Text = "Promedio de horas requeridas para emitir  " +
                    //        "un acuerdo derivado de la recepción de una actuación";
                    //    valor1ind7.Text = "Fecha de recepción de la actuación (+1): " + li[li.Count - 1].Val1.ToString();
                    //    valor2ind7.Text = "Fecha de emisión del acuerdo: " + li[li.Count - 1].Val2.ToString();
                    //    calculoind7.Text = "Promedio en horas : " + li[li.Count - 1].Calculo.ToString();

                    //    tituloL = "Emitir un acuerdo derivado de la recepción de una actuación";
                    //    tituloLineas = "Promedio de horas";
                    //    yTitulo = "Proporcion en horas";

                    //    String fn7 = "LineasGeneral(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                    //        + tituloLineas + "', '" + yTitulo + "','lineas_ind7'); ";
                    //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "LG7", fn7, true);

                    //    break;

                    //case 8:

                    //    li = Indicadores.TraeInformacion(3, anio, mes, distrito, juzgado);

                    //    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    //    indicador.Text = "Promedio de horas requeridas para notificar personalmente un acuerdo ";
                    //    valor1ind8.Text = "Fecha de emisión del acuerdo (+1): " + li[li.Count - 1].Val1.ToString();
                    //    valor2ind8.Text = "Fecha de la notificación: " + li[li.Count - 1].Val2.ToString();
                    //    calculoind8.Text = "Promedio en horas: " + li[li.Count - 1].Calculo.ToString();

                    //    tituloL = "Notificar personalmente un acuerdo";
                    //    tituloLineas = "Promedio de horas";
                    //    yTitulo = "Proporcion en horas";

                    //    String fn8 = "LineasGeneral(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                    //        + tituloLineas + "', '" + yTitulo + "','lineas_ind8'); ";
                    //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "LG8", fn8, true);

                    //    break;

                    //case 9:

                    //    li = Indicadores.TraeInformacion(4, anio, mes, distrito, juzgado);

                    //    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    //    indicador.Text = "Promedio de audiencias efectivamente celebradas en relación con  " +
                    //        "las audiencias señaladas buscando contrarestar el diferimiento innecesario";
                    //    valor1ind9.Text = "Total de audiencias celebradas: " + li[li.Count - 1].Val1.ToString();
                    //    valor2ind9.Text = "Total de audiencias programadas: " + li[li.Count - 1].Val2.ToString();
                    //    calculoind9.Text = "Promedio de Audiencias: " + li[li.Count - 1].Calculo.ToString();

                    //    tituloL = "Promedio de audiencias efectivamente celebradas";
                    //    tituloLineas = "Promedio de Audiencias";
                    //    yTitulo = "Proporcion en Audiencias";

                    //    String fn9 = "LineasGeneral(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                    //        + tituloLineas + "', '" + yTitulo + "','lineas_ind9'); ";
                    //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "LG9", fn9, true);

                    //    break;

                    //case 10:

                    //    li = Indicadores.TraeInformacion(5, anio, mes, distrito, juzgado);

                    //    distritojugado.Text = "DISTRITO " + distritos.SelectedItem.Text + " " + nomuzgado;
                    //    indicador.Text = "Proporción de asuntos que tienen una resolución cumplida.  ";
                    //    valor1ind10.Text = "Total de expedientes: " + li[li.Count - 1].Val1.ToString();
                    //    valor2ind10.Text = "Total de resoluciones cumplidas: " + li[li.Count - 1].Val2.ToString();
                    //    calculoind10.Text = "Porcentaje de Asuntos: " + li[li.Count - 1].Calculo.ToString();

                    //    tituloL = "Asuntos que tienen una resolución cumplida";
                    //    tituloLineas = "Porcentaje de Asuntos";
                    //    yTitulo = "Proporcion de Asuntos";

                    //    String fn10 = "LineasGeneral(" + JsonConvert.SerializeObject(li) + ", '" + tituloL + "', '"
                    //        + tituloLineas + "', '" + yTitulo + "','lineas_ind10'); ";
                    //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "LG10", fn10, true);

                    //    break;

                }
            }

            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "PDF", "exporta();", true);
        }

        public void CierraSesion(object sender, EventArgs e)
        {
            String retorno = ((List<String>)Session["acceso"])[1];
            Session.Remove("acceso");
            Response.Redirect("http://gestionjudicial.pjedomex.gob.mx/ideas/laboral/proceso/atencion-al-publico");
        }

        public void CambiaMes(object sender, EventArgs e)
        {
            meses.Items.Clear();

            string[] mesinyear = { "", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
            int minmes = 1;
            int mesMax = (DateTime.Now.Month == 1) ? 12 : (DateTime.Now.Month - 1);

            if (anios.SelectedValue == "2020")
            {
                minmes = 11;
                link_detalle.Enabled = false;
                link_detalle.Visible = false;
            }
            else
            {
                link_detalle.Enabled = true;
                link_detalle.Visible = true;
            }

            if (int.Parse(anios.SelectedValue) < DateTime.Now.Year)
            {
                mesMax = 12;
            }


            for (int mes = minmes; mes <= mesMax; mes++)
            {
                ListItem l = new ListItem();
                l.Text = mesinyear[mes];
                l.Value = mes.ToString();
                meses.Items.Add(l);
            }

           meses.SelectedValue = mesMax.ToString();
        }

        public void GeneraDetalle(object sender, EventArgs e)
        {
            if (anios.SelectedValue == "2020")
            {
               
            }
            else
            {
                int anio = int.Parse(anios.SelectedValue);
                int mes = int.Parse(meses.SelectedValue);
                String distrito = distritos.SelectedValue == "0" ? "" : distritos.SelectedValue;
                String juzgado = juzgados.SelectedValue == "0" ? "" : juzgados.SelectedValue;
                String nomuzgado = juzgados.SelectedValue == "0" ? "Totalidad de Tribunales" : juzgados.SelectedItem.Text;

                Detalles dt = new Detalles();
                ExcelPackage x = dt.GeneraExcel(anio, mes, distrito, juzgado);
                MemoryStream memoryStream = new MemoryStream();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=Detalles_Indicadores " + DateTime.Today.ToString("ddMMyyyy") + ".xlsx");
                x.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            
        }
    }
}